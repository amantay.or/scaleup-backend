<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained()->onDelete('cascade');
            $table->integer('r_strategy')->default(0);
            $table->integer('r_financial')->default(0);
            $table->integer('r_legal')->default(0);
            $table->integer('r_marketing')->default(0);
            $table->integer('strategy')->default(0);
            $table->integer('financial')->default(0);
            $table->integer('legal')->default(0);
            $table->integer('marketing')->default(0);
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productions');
    }
}
