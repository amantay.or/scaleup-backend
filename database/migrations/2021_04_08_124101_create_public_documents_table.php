<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePublicDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('public_documents', function (Blueprint $table) {
            $table->id();
            $table->string('strategy')->default(0);
            $table->string('financial')->default(0);
            $table->string('legal')->default(0);
            $table->string('marketing')->default(0);
            $table->foreignId('user_id')->constrained()->onDelete('cascade');
            $table->string('token')->unique();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('public_documents');
    }
}
