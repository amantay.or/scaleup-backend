<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Form;

class LegalFormSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Form::create([
            'question_id'=>'1',
            'title'=>'Название компании:',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-1',
        ]);
        Form::create([
            'question_id'=>'1',
            'title'=>'Отрасль компании',
            'placeholder'=>'',
            'type'=>2,
            'key'=>'legal-2',
            'answers'=>json_encode([
                "ans-1"=>'Ритейл',
                "ans-2"=>'Образование',
                "ans-3"=>'Общественное питание',
                "ans-4"=>'Услуги',
                "ans-5"=>'Производство',
                "ans-6"=>'Развлечение',

            ])
        ]);
        Form::create([
            'question_id'=>'2',
            'title'=>'Страна:',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-3',
        ]);
        Form::create([
            'question_id'=>'2',
            'title'=>'Город (нас. пункт):',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-4',
        ]);
        Form::create([
            'question_id'=>'2',
            'title'=>'Улица (проспект):',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-5',
        ]);
        Form::create([
            'question_id'=>'2',
            'title'=>'Номер дома:',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-6',
        ]);
        Form::create([
            'question_id'=>'2',
            'title'=>'Номер офиса:',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-7',
        ]);
        Form::create([
            'question_id'=>'2',
            'title'=>'Наименование бизнес-центра:',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-8',
        ]);
        Form::create([
            'question_id'=>'3',
            'title'=>'Номер телефона №1',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-9',
            'input'=>'phone'
        ]);
        Form::create([
            'question_id'=>'3',
            'title'=>'Номер телефона №2',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-10',
            'input'=>'phone'
        ]);
        Form::create([
            'question_id'=>'3',
            'title'=>'Номер телефона №3',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-11',
            'input'=>'phone'
        ]);
        Form::create([
            'question_id'=>'4',
            'title'=>'E-mail',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-12',
            'input'=>'email',
        ]);
        Form::create([
            'question_id'=>'5',
            'title'=>'Банк:',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-13',
        ]);
        Form::create([
            'question_id'=>'5',
            'title'=>'БИН:',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-14',
            'input'=>'number',
        ]);
        Form::create([
            'question_id'=>'5',
            'title'=>'ИИК:',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-15',
        ]);
        Form::create([
            'question_id'=>'5',
            'title'=>'Расчетный счёт:',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-16',
            
        ]);
        Form::create([
            'question_id'=>'5',
            'title'=>'БИК:',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-121',
            
        ]);
        Form::create([
            'question_id'=>'6',
            'title'=>'Фамилия',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-17',
        ]);
        Form::create([
            'question_id'=>'6',
            'title'=>'Имя',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-18',
        ]);
        Form::create([
            'question_id'=>'6',
            'title'=>'Отчество',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-19',
        ]);
        Form::create([
            'question_id'=>'6',
            'title'=>'Напишите ФИО в родительном падеже. Пример: Бикамалова Аскара Нурсултановича, Жасболатовой Акнур Сериковной',
            'placeholder'=>'',
            'type'=>1,
            'key'=>'legal-124',
        ]);

        Form::create([
            'question_id'=>'6',
            'title'=>'Должность',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-20',
        ]);
        Form::create([
            'question_id'=>'6',
            'title'=>'Пол',
            'type'=>2,
            'key'=>'legal-21',
            'answers'=>json_encode([
                "ans-1"=>'Мужской',
                "ans-2"=>'Женский',
            ])
        ]);
        Form::create([
            'question_id'=>'7',
            'title'=>'Название юридического документа',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-22',
        ]);
        Form::create([
            'question_id'=>'8',
            'title'=>'Программа №1',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-23',
        ]);
        Form::create([
            'question_id'=>'8',
            'title'=>'Программа №2',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-24',
        ]);
        Form::create([
            'question_id'=>'8',
            'title'=>'Программа №3',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-25',
        ]);
        Form::create([
            'question_id'=>'9',
            'title'=>'Документ №1',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-26',
        ]);
        Form::create([
            'question_id'=>'9',
            'title'=>'Документ №2',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-27',
        ]);
        Form::create([
            'question_id'=>'9',
            'title'=>'Документ №3',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-28',
        ]);
        Form::create([
            'question_id'=>'9',
            'title'=>'Документ №4',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-29',
        ]);
        Form::create([
            'question_id'=>'9',
            'title'=>'Документ №5',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-30',
        ]);
        Form::create([
            'question_id'=>'10',
            'title'=>'Разработка/методика №1',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-31',
            'input'=>'textarea'
        ]);
        Form::create([
            'question_id'=>'10',
            'title'=>'Разработка/методика №2',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-32',
            'input'=>'textarea'
        ]);
        Form::create([
            'question_id'=>'10',
            'title'=>'Разработка/методика №3',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-33',
            'input'=>'textarea'
        ]);
        Form::create([
            'question_id'=>'10',
            'title'=>'Разработка/методика №4',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-34',
            'input'=>'textarea'
        ]);
        Form::create([
            'question_id'=>'10',
            'title'=>'Разработка/методика №5',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-35',
            'input'=>'textarea'
        ]);
        Form::create([
            'question_id'=>'11',
            'title'=>'ТМЦ (musthave) №1',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-36',
            'input'=>'textarea'
        ]);
        Form::create([
            'question_id'=>'11',
            'title'=>'ТМЦ (musthave) №2',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-37',
            'input'=>'textarea'
        ]);
        Form::create([
            'question_id'=>'11',
            'title'=>'ТМЦ (musthave) №3',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-38',
            'input'=>'textarea'
        ]);
        Form::create([
            'question_id'=>'11',
            'title'=>'ТМЦ (musthave) №4',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-39',
            'input'=>'textarea'
        ]);
        Form::create([
            'question_id'=>'11',
            'title'=>'ТМЦ (musthave) №5',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-40',
            'input'=>'textarea'
        ]);
        Form::create([
            'question_id'=>'11',
            'title'=>'Загрузите пожалуйста фото требуемого ТМЦ',
            'placeholder'=>'Мой ответ',
            'type'=>3,
            'key'=>'legal-41',
            'input'=>'textarea'
        ]);
        Form::create([
            'question_id'=>'11',
            'title'=>'Загрузите пожалуйста фото требуемого ТМЦ',
            'placeholder'=>'Мой ответ',
            'type'=>3,
            'key'=>'legal-42',
            'input'=>'textarea'
        ]);
        Form::create([
            'question_id'=>'11',
            'title'=>'Загрузите пожалуйста фото требуемого ТМЦ',
            'placeholder'=>'Мой ответ',
            'type'=>3,
            'key'=>'legal-43',
            'input'=>'textarea'
        ]);
        Form::create([
            'question_id'=>'11',
            'title'=>'Загрузите пожалуйста фото требуемого ТМЦ',
            'placeholder'=>'Мой ответ',
            'type'=>3,
            'key'=>'legal-44',
            'input'=>'textarea'
        ]);
        Form::create([
            'question_id'=>'11',
            'title'=>'Загрузите пожалуйста фото требуемого ТМЦ',
            'placeholder'=>'Мой ответ',
            'type'=>3,
            'key'=>'legal-45',
            'input'=>'textarea'
        ]);
        Form::create([
            'question_id'=>'12',
            'title'=>'Характеристики помещения (можно через запятую):',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-46',
            'input'=>'textarea'
        ]);
        Form::create([
            'question_id'=>'13',
            'title'=>'Наименование должности',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-47',
        ]);
        Form::create([
            'question_id'=>'13',
            'title'=>'Количество человек в указанной должности',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-48',
            'input'=>'number',
        ]);
        Form::create([
            'question_id'=>'13',
            'title'=>'Наименование должности',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-49',
        ]);
        Form::create([
            'question_id'=>'13',
            'title'=>'Количество человек в указанной должности',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-50',
            'input'=>'number',
        ]);
        Form::create([
            'question_id'=>'13',
            'title'=>'Наименование должности',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-51',
        ]);
        Form::create([
            'question_id'=>'13',
            'title'=>'Количество человек в указанной должности',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-52',
            'input'=>'number',
        ]);
        Form::create([
            'question_id'=>'13',
            'title'=>'Наименование должности',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-53',
        ]);
        Form::create([
            'question_id'=>'13',
            'title'=>'Количество человек в указанной должности',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-54',
            'input'=>'number',
        ]);
        Form::create([
            'question_id'=>'13',
            'title'=>'Наименование должности',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-55',
        ]);
        Form::create([
            'question_id'=>'13',
            'title'=>'Количество человек в указанной должности',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-56',
            'input'=>'number',
        ]);
        Form::create([
            'question_id'=>'13',
            'title'=>'Наименование должности',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-57',
        ]);
        Form::create([
            'question_id'=>'13',
            'title'=>'Количество человек в указанной должности',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-58',
            'input'=>'number',
        ]);
        Form::create([
            'question_id'=>'14',
            'title'=>'Должность:',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-59',
        ]);
        Form::create([
            'question_id'=>'14',
            'title'=>'Должность:',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-60',
        ]);
        Form::create([
            'question_id'=>'14',
            'title'=>'Должность:',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-61',
        ]);
        Form::create([
            'question_id'=>'14',
            'title'=>'Должность:',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-62',
        ]);
        Form::create([
            'question_id'=>'14',
            'title'=>'Должность:',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-63',
        ]);
        Form::create([
            'question_id'=>'15',
            'title'=>'Должность',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-64',
        ]);
        Form::create([
            'question_id'=>'15',
            'title'=>'Возраст',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-65',
            'input'=>'number',
        ]);
        Form::create([
            'question_id'=>'15',
            'title'=>'Характеристика к должности (можно через запятую)',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-66',
            'input'=>'textarea'
        ]);
        Form::create([
            'question_id'=>'15',
            'title'=>'Требования к навыкам',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-67',
            'input'=>'textarea'
        ]);
        Form::create([
            'question_id'=>'15',
            'title'=>'Пол',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-68',
        ]);
        Form::create([
            'question_id'=>'15',
            'title'=>'Знание языков',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-69',
        ]);
        Form::create([
            'question_id'=>'16',
            'title'=>'Описание внешнего вида',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-70',
            'input'=>'textarea'
        ]);
        Form::create([
            'question_id'=>'17',
            'title'=>'Форма сотрудника',
            'placeholder'=>'Мой ответ',
            'type'=>3,
            'key'=>'legal-71',
        ]);
        Form::create([
            'question_id'=>'18',
            'title'=>'Номер телефона',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-72',
            'input'=>'phone',
        ]);
        Form::create([
            'question_id'=>'18',
            'title'=>'Номер телефона №2',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-73',
            'input'=>'phone',
        ]);
        Form::create([
            'question_id'=>'18',
            'title'=>'Электронная почта',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-74',
            'input'=>'email',
        ]);
        Form::create([
            'question_id'=>'18',
            'title'=>'WhatsApp',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-75',
            'input'=>'phone',
        ]);
        Form::create([
            'question_id'=>'18',
            'title'=>'Telegram',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-76',
        ]);
        Form::create([
            'question_id'=>'18',
            'title'=>'Instagram',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-77',
        ]);
        Form::create([
            'question_id'=>'18',
            'title'=>'Сайт',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-78',
        ]);
        Form::create([
            'question_id'=>'18',
            'title'=>'Facebook',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-79',
        ]);
        Form::create([
            'question_id'=>'19',
            'title'=>'Номер телефона',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-80',
            'input'=>'phone',
        ]);
        Form::create([
            'question_id'=>'19',
            'title'=>'Номер телефона №2',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-81',
            'input'=>'phone',
        ]);
        Form::create([
            'question_id'=>'19',
            'title'=>'Электронная почта',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-82',
            'input'=>'email',
        ]);
        Form::create([
            'question_id'=>'19',
            'title'=>'WhatsApp',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-83',
            'input'=>'phone'
        ]);
        Form::create([
            'question_id'=>'19',
            'title'=>'Telegram',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-84',
        ]);
        Form::create([
            'question_id'=>'19',
            'title'=>'Instagram',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-85',
        ]);
        Form::create([
            'question_id'=>'19',
            'title'=>'Facebook',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-86',
        ]);
        Form::create([
            'question_id'=>'19',
            'title'=>'Сайт',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-87',
        ]);
        Form::create([
            'question_id'=>'20',
            'title'=>'Размер штрафа',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-88',
        ]);
        Form::create([
            'question_id'=>'20',
            'title'=>'Наименование валюты',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-89',
        ]);
        Form::create([
            'question_id'=>'21',
            'title'=>'Размер штрафа',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-90',
        ]);
        Form::create([
            'question_id'=>'21',
            'title'=>'Наименование валюты',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-91',
        ]);
        Form::create([
            'question_id'=>'22',
            'title'=>'Размер штрафа',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-92',
        ]);
        Form::create([
            'question_id'=>'22',
            'title'=>'Наименование валюты',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-93',
        ]);
        Form::create([
            'question_id'=>'23',
            'title'=>'Размер штрафа:',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-94',
        ]);
        Form::create([
            'question_id'=>'23',
            'title'=>'Наименование валюты',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-95',
        ]);
        Form::create([
            'question_id'=>'24',
            'title'=>'Наименование нарушения',
            'placeholder'=>'Мой ответ',
            'type'=>4,
            'key'=>'legal-96',
            'answers'=>json_encode([
                "ans-1"=>'Hазмещение рекламы без символики, логотипа Франчайзера',
                "ans-2"=>'Cовершения разбоя, беспорядка персоналом Франчайзи',
                "ans-3"=>'Несоблюдения Франчайзи требований Франчайзера о количестве публикаций в СМИ, в сети Интернет, согласно SMM плана',
                "ans-4"=>'Несоблюдение работниками Франчайзи этических манер общения, вульгарного дресс-кода, установленного Франчайзером',
                "ans-5"=>'Неиспользование Франчайзи в публичных выступлениях или в публикациях указания на франчайзинговую сеть',
                "ans-6"=>'Другое:',

            ])
        ]);
        Form::create([
            'question_id'=>'24',
            'title'=>'Размер штрафа или иные условия',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-97',
        ]);
        Form::create([
            'question_id'=>'24',
            'title'=>'Валюта штрафа',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-98',
        ]);
        Form::create([
            'question_id'=>'24',
            'title'=>'Наименование нарушения',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-99',
        ]);
        Form::create([
            'question_id'=>'24',
            'title'=>'Размер штрафа или иные условия',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-100',
        ]);
        Form::create([
            'question_id'=>'24',
            'title'=>'Валюта штрафа',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-101',
        ]);
        Form::create([
            'question_id'=>'24',
            'title'=>'Наименование нарушения',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-102',
        ]);
        Form::create([
            'question_id'=>'24',
            'title'=>'Размер штрафа или иные условия',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-103',
        ]);
        Form::create([
            'question_id'=>'24',
            'title'=>'Валюта штрафа',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-104',
        ]);
        Form::create([
            'question_id'=>'24',
            'title'=>'Наименование нарушения',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-105',
        ]);
        Form::create([
            'question_id'=>'24',
            'title'=>'Размер штрафа или иные условия',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-106',
        ]);
        Form::create([
            'question_id'=>'24',
            'title'=>'Валюта штрафа',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-107',
        ]);
        Form::create([
            'question_id'=>'24',
            'title'=>'Наименование нарушения',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-108',
        ]);
        Form::create([
            'question_id'=>'24',
            'title'=>'Размер штрафа или иные условия',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-109',
        ]);
        Form::create([
            'question_id'=>'24',
            'title'=>'Валюта штрафа',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-110',
        ]);
        Form::create([
            'question_id'=>'25',
            'title'=>'Наименование нарушения',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-111',
        ]);
        Form::create([
            'question_id'=>'25',
            'title'=>'Размер штрафа или иные условия',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-112',
        ]);
        Form::create([
            'question_id'=>'25',
            'title'=>'Валюта штрафа',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-113',
        ]);
        Form::create([
            'question_id'=>'25',
            'title'=>'Наименование нарушения',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-114',
        ]);
        Form::create([
            'question_id'=>'25',
            'title'=>'Размер штрафа или иные условия',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-115',
        ]);
        Form::create([
            'question_id'=>'25',
            'title'=>'Валюта штрафа',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-116',
        ]);
        Form::create([
            'question_id'=>'25',
            'title'=>'Наименование нарушения',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-117',
        ]);
        Form::create([
            'question_id'=>'25',
            'title'=>'Размер штрафа или иные условия',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-118',
        ]);
        Form::create([
            'question_id'=>'25',
            'title'=>'Валюта штрафа',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-119',
        ]);
        Form::create([
            'question_id'=>'26',
            'title'=>'Наименование условия:',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-120',
            'input'=>'textarea'
        ]);  
        Form::create([
            'question_id'=>'6',
            'title'=>'Укажите ИИН',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-122',
        ]);
        Form::create([
            'question_id'=>'27',
            'title'=>'Какие IT-системы вы используете для учета и контроля? Перечислите через запятую.',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'legal-123',
        ]);


        //EMPTY

        Form::create([
            'question_id'=>'67',
            'title'=>'',
            'placeholder'=>'',
            'type'=>1,
            'key'=>'legal-125',
        ]);

        Form::create([
            'question_id'=>'67',
            'title'=>'',
            'placeholder'=>'',
            'type'=>1,
            'key'=>'legal-126',
        ]);

        Form::create([
            'question_id'=>'67',
            'title'=>'',
            'placeholder'=>'',
            'type'=>1,
            'key'=>'legal-127',
        ]);

        Form::create([
            'question_id'=>'67',
            'title'=>'',
            'placeholder'=>'',
            'type'=>1,
            'key'=>'legal-128',
        ]);

        Form::create([
            'question_id'=>'67',
            'title'=>'',
            'placeholder'=>'',
            'type'=>1,
            'key'=>'legal-129',
        ]);


    }
}
