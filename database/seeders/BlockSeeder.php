<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\BLock;

class BlockSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BLock::create(['greeting'=>'Hello World','image'=>'images/block/block-image.jpg','description'=>'try smth..','link'=>'https://google.com']);
    }
}
