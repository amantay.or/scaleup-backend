<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create(['name'=>'role-edit','guard_name'=>'api']);//1
        Permission::create(['name'=>'accepted','guard_name'=>'api']);//2
        Permission::create(['name'=>'service-edit','guard_name'=>'api']);//3
        Permission::create(['name'=>'top-faq-edit','guard_name'=>'api']);//4
        Permission::create(['name'=>'faq-category-edit','guard_name'=>'api']);//5
        Permission::create(['name'=>'faq-edit','guard_name'=>'api']);//6
        Permission::create(['name'=>'payment-control','guard_name'=>'api']);//7
        Permission::create(['name'=>'worker','guard_name'=>'api']);//8
    }
}
