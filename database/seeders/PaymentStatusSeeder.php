<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\PaymentStatus;

class PaymentStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PaymentStatus::create([
        'name'=>'Платеж создан',
        'description'=>'Платежная транзакция еще не до конца создана, например, не определен платежный метод.',
        'keyword'=>'created']);

        PaymentStatus::create([
        'name'=>'В ожидании платежа',
        'description'=>'Платежная транзакция создана и ждет оплаты. Состояние платежа может перейти в состояние Отозванa',
        'keyword'=>'pending']);

        PaymentStatus::create([
        'name'=>'Платеж принят',
        'description'=>'Платеж завершился успешно.',
        'keyword'=>'accepted']);

        PaymentStatus::create([
        'name'=>'Платеж не прошел',
        'description'=>'Ошибка при проведении платежа. Подробнее смотреть в переходах состоянии транзакции.',
        'keyword'=>'failed']);

        PaymentStatus::create([
        'name'=>'Платеж отозван',
        'description'=>'Платеж прошел успешно, но затем был отозван. Например: истекла бронь на место.',
        'keyword'=>'revoked']);

        PaymentStatus::create([
        'name'=>'Платеж отменен',
        'description'=>'Отмена платежа.',
        'keyword'=>'canceled']);

        
    }
}
