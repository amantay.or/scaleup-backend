<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'fio'     =>'Admin Admin',
            'company' =>'admin.kz',
            'phone'   =>'87768222415',
            'email'   =>'admin@gmail.com',
            'email_verified_at'=>'2021-03-04 00:00:00',
            'password'=>bcrypt('12345678'),
            'docs'=>1,
        ]); 
        $user->assignRole(Role::find(8));


        $user = User::create([
            'fio'     =>'Sale Department',
            'company' =>'sale_department.kz',
            'phone'   =>'87768222415',
            'email'   =>'sale_department@gmail.com',
            'email_verified_at'=>'2021-03-04 00:00:00',
            'password'=>bcrypt('12345678'),
            'docs'=>1,
        ]); 
        $user->assignRole(Role::find(7));

        $user = User::create([
            'fio'     =>'moderator moderator',
            'company' =>'moderator.kz',
            'phone'   =>'87768222415',
            'email'   =>'moderator@gmail.com',
            'email_verified_at'=>'2021-03-04 00:00:00',
            'password'=>bcrypt('12345678'),
            'docs'=>1,
        ]); 
        $user->assignRole(Role::find(6));

        $user = User::create([
            'fio'     =>'marketer marketer',
            'company' =>'marketer.kz',
            'phone'   =>'87768222415',
            'email'   =>'marketer@gmail.com',
            'email_verified_at'=>'2021-03-04 00:00:00',
            'password'=>bcrypt('12345678'),
            'docs'=>1,
        ]); 
        $user->assignRole(Role::find(5));


        $user = User::create([
            'fio'     =>'lawyer lawyer',
            'company' =>'lawyer.kz',
            'phone'   =>'87768222415',
            'email'   =>'lawyer@gmail.com',
            'email_verified_at'=>'2021-03-04 00:00:00',
            'password'=>bcrypt('12345678'),
            'docs'=>1,
        ]); 
        $user->assignRole(Role::find(4));


        $user = User::create([
            'fio'     =>'financier financier',
            'company' =>'financier.kz',
            'phone'   =>'87768222415',
            'email'   =>'financier@gmail.com',
            'email_verified_at'=>'2021-03-04 00:00:00',
            'password'=>bcrypt('12345678'),
            'docs'=>1,
        ]); 
        $user->assignRole(Role::find(3));


        $user = User::create([
            'fio'     =>'editor editor',
            'company' =>'editor.kz',
            'phone'   =>'87768222415',
            'email'   =>'editor@gmail.com',
            'email_verified_at'=>'2021-03-04 00:00:00',
            'password'=>bcrypt('12345678'),
            'docs'=>1,
        ]); 
        $user->assignRole(Role::find(2));

        $user = User::create([
            'fio'     =>'Amantay Amantay',
            'company' =>'amantay.kz',
            'phone'   =>'87768222415',
            'email'   =>'amantay.or@gmail.com',
            'email_verified_at'=>'2021-03-04 00:00:00',
            'password'=>bcrypt('12345678')
        ]); 
        $user->assignRole(Role::find(1));

        $user = User::create([
            'fio'     =>'Madi Madi',
            'company' =>'madi.kz',
            'phone'   =>'87768222415',
            'email'   =>'madibika@gmail.com',
            'email_verified_at'=>'2021-03-04 00:00:00',
            'password'=>bcrypt('madi')
        ]); 
        $user->assignRole(Role::find(1));

        $user = User::create([
            'fio'     =>'Aselhan Aselhan',
            'company' =>'Aselhan.kz',
            'phone'   =>'87768222415',
            'email'   =>'aselhan@gmail.com',
            'email_verified_at'=>'2021-03-04 00:00:00',
            'password'=>bcrypt('12345678')
        ]); 
        $user->assignRole(Role::find(1));

    }
}
