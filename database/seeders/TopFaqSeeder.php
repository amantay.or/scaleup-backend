<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\TopFaq;

class TopFaqSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TopFaq::create(['title'=>'Регистрация товарного знака', 'text'=>'Text 1']);
        TopFaq::create(['title'=>'Получите новый поток прибыли от существующего бизнеса', 'text'=>'Text 2']);
        TopFaq::create(['title'=>'Патент франшизы', 'text'=>'Text 3']);
        TopFaq::create(['title'=>'Рукводство по использованию', 'text'=>'Text 4']);
        TopFaq::create(['title'=>'Получите новый поток прибыли от существующего бизнеса', 'text'=>'Text 5']);
    }
}
