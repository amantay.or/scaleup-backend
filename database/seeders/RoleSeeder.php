<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create(['name'=>'client','guard_name'=>'api']);
        Role::create(['name'=>'editor','guard_name'=>'api']);
        Role::create(['name'=>'financier','guard_name'=>'api']);
        Role::create(['name'=>'lawyer','guard_name'=>'api']);
        Role::create(['name'=>'marketer','guard_name'=>'api']);
        Role::create(['name'=>'moderator','guard_name'=>'api']);
        Role::create(['name'=>'sale_department','guard_name'=>'api']);
        Role::create(['name'=>'admin','guard_name'=>'api']);
    }
}
