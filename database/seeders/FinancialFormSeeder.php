<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Form;

class FinancialFormSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Form::create([
            'question_id'=>'40',
            'title'=>'Вставьте ссылку на сайт или аккаунты в соцсетях, если они имеются ',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'financial-41'
        ]);//1
        Form::create([
            'question_id'=>'41',
            'title'=>'1 месяц',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'financial-1',
            'input'=>'number'
        ]);
        Form::create([
            'question_id'=>'41',
            'title'=>'2 месяц',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'financial-2',
            'input'=>'number'
        ]);
        Form::create([
            'question_id'=>'41',
            'title'=>'3 месяц',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'financial-3',
            'input'=>'number'
        ]);//3
        Form::create([
            'question_id'=>'42',
            'title'=>'Переменные затраты',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'financial-40',
            'input'=>'textarea'
        ]);//3
        Form::create([
            'question_id'=>'43',
            'title'=>'1 месяц',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'financial-4',
            'input'=>'number'
        ]);
        Form::create([
            'question_id'=>'43',
            'title'=>'2 месяц',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'financial-5',
            'input'=>'number'
        ]);
        Form::create([
            'question_id'=>'43',
            'title'=>'3 месяц',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'financial-6',
            'input'=>'number'
        ]);
        Form::create([
            'question_id'=>'44',
            'title'=>'1 месяц',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'financial-7',
            'input'=>'number'
        ]);
        Form::create([
            'question_id'=>'44',
            'title'=>'2 месяц',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'financial-8',
            'input'=>'number'
        ]);
        Form::create([
            'question_id'=>'44',
            'title'=>'3 месяц',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'financial-9',
            'input'=>'number'
        ]);
        Form::create([
            'question_id'=>'45',
            'title'=>'1 месяц',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'financial-10',
            'input'=>'number'
        ]);
        Form::create([
            'question_id'=>'45',
            'title'=>'2 месяц',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'financial-11',
            'input'=>'number'
        ]);
        Form::create([
            'question_id'=>'45',
            'title'=>'3 месяц',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'financial-12',
            'input'=>'number'
        ]);
        Form::create([
            'question_id'=>'45',
            'title'=>'Укажите роялти в %',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'financial-13',
            'input'=>'number'
        ]);//4
        Form::create([
            'question_id'=>'46',
            'title'=>'1 месяц',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'financial-14',
            'input'=>'textarea'
        ]);//1
        Form::create([
            'question_id'=>'47',
            'title'=>'1 месяц',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'financial-39',
            'input'=>'number'
        ]);
        Form::create([
            'question_id'=>'47',
            'title'=>'2 месяц',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'financial-15',
            'input'=>'number'
        ]);
        Form::create([
            'question_id'=>'47',
            'title'=>'3 месяц',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'financial-16',
            'input'=>'number'
        ]);//3
        Form::create([
            'question_id'=>'48',
            'title'=>'1 месяц',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'financial-17',
            'input'=>'number'
        ]);
        Form::create([
            'question_id'=>'48',
            'title'=>'2 месяц',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'financial-18',
            'input'=>'number'
        ]);
        Form::create([
            'question_id'=>'48',
            'title'=>'3 месяц',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'financial-19',
            'input'=>'number'
        ]);//3
        Form::create([
            'question_id'=>'49',
            'title'=>'1 месяц',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'financial-20',
            'input'=>'number'
        ]);
        Form::create([
            'question_id'=>'49',
            'title'=>'2 месяц',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'financial-21',
            'input'=>'number'
        ]);
        Form::create([
            'question_id'=>'49',
            'title'=>'3 месяц',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'financial-22',
            'input'=>'number'
        ]);//
        Form::create([
            'question_id'=>'50',
            'title'=>'1 месяц',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'financial-23',
            'input'=>'number'
        ]);
        Form::create([
            'question_id'=>'50',
            'title'=>'2 месяц',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'financial-24',
            'input'=>'number'
        ]);
        Form::create([
            'question_id'=>'50',
            'title'=>'3 месяц',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'financial-25',
            'input'=>'number'
        ]);//3
        Form::create([
            'question_id'=>'51',
            'title'=>'1 месяц',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'financial-26',
            'input'=>'number'
        ]);
        Form::create([
            'question_id'=>'51',
            'title'=>'2 месяц',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'financial-27',
            'input'=>'number'
        ]);
        Form::create([
            'question_id'=>'51',
            'title'=>'3 месяц',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'financial-28',
            'input'=>'number'
        ]);//3
        Form::create([
            'question_id'=>'52',
            'title'=>'1 месяц',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'financial-29',
            'input'=>'number'
        ]);
        Form::create([
            'question_id'=>'52',
            'title'=>'2 месяц',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'financial-30',
            'input'=>'number'
        ]);
        Form::create([
            'question_id'=>'52',
            'title'=>'3 месяц',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'financial-31',
            'input'=>'number'
        ]);//3
        Form::create([
            'question_id'=>'53',
            'title'=>'Распишите пожалуйста на какие позиции должен будет вложить инвестиции франчайзи:',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'financial-32',
            'input'=>'textarea'
        ]);
        Form::create([
            'question_id'=>'53',
            'title'=>'Всего:',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'financial-33'
        ]);//2
        Form::create([
            'question_id'=>'54',
            'title'=>'Распишите пожалуйста позиции оборотных средств',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'financial-34',
            'input'=>'textarea'
        ]);
        Form::create([
            'question_id'=>'54',
            'title'=>'Всего:',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'financial-35'
        ]);//2
        Form::create([
            'question_id'=>'55',
            'title'=>'Укажите стоимость бизнеса',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'financial-36'
        ]);//1
        Form::create([
            'question_id'=>'56',
            'title'=>'Укажите %',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'financial-37'
        ]);//1
        Form::create([
            'question_id'=>'57',
            'title'=>'Укажите %',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'financial-38'
        ]);//1
       

        //EMPTY

        Form::create([
            'question_id'=>'66',
            'title'=>'',
            'placeholder'=>'',
            'type'=>1,
            'key'=>'financial-42',
        ]);

        Form::create([
            'question_id'=>'66',
            'title'=>'',
            'placeholder'=>'',
            'type'=>1,
            'key'=>'financial-43',
        ]);

        Form::create([
            'question_id'=>'66',
            'title'=>'',
            'placeholder'=>'',
            'type'=>1,
            'key'=>'financial-44',
        ]);

        Form::create([
            'question_id'=>'66',
            'title'=>'',
            'placeholder'=>'',
            'type'=>1,
            'key'=>'financial-45',
        ]);

        Form::create([
            'question_id'=>'66',
            'title'=>'',
            'placeholder'=>'',
            'type'=>1,
            'key'=>'financial-46',
        ]);
        
    }
}
