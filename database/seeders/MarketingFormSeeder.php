<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Form;

class MarketingFormSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Form::create([
            'question_id'=>'58',
            'title'=>'Имеется ли маркетинговая стратегия компании?',
            'placeholder'=>'Мой ответ',
            'type'=>2,
            'key'=>'marketing-1',
            'answers'=>json_encode([
                "ans-1"=>'Да',
                "ans-2"=>'Нет',
            ])
        ]);

        Form::create([
            'question_id'=>'58',
            'title'=>'Если есть маркетинговая компания загрузите её',
            'placeholder'=>'Мой ответ',
            'type'=>3,
            'key'=>'marketing-2',
            'input'=>'number',
        ]);

        Form::create([
            'question_id'=>'58',
            'title'=>'Какая основная миссия компании ?',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'marketing-3',
            'input'=>'textarea'
        ]);

        Form::create([
            'question_id'=>'58',
            'title'=>'Каких ценностей придерживается компания: укажите список ценностей выбранных вашей компанией, с описанием',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'marketing-4',
            'input'=>'textarea'
        ]);

        Form::create([
            'question_id'=>'58',
            'title'=>'Какая стратегия компании на ближайшие 5 лет?',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'marketing-5',
            'input'=>'textarea'
        ]);

        Form::create([
            'question_id'=>'59',
            'title'=>'Загрузите логотип',
            'placeholder'=>'Мой ответ',
            'type'=>3,
            'key'=>'marketing-6',
        ]);

        Form::create([
            'question_id'=>'59',
            'title'=>'Загрузите фирменный бланк / визитку /брэнд бук',
            'placeholder'=>'Мой ответ',
            'type'=>3,
            'key'=>'marketing-7',
        ]);

        Form::create([
            'question_id'=>'59',
            'title'=>'Имеется ли у вас стандарты мерчендайзинга?',
            'placeholder'=>'Мой ответ',
            'type'=>2,
            'key'=>'marketing-8',
            'answers'=>json_encode([
                "ans-1"=>'Да',
                "ans-2"=>'Нет',
            ])
        ]);

        Form::create([
            'question_id'=>'59',
            'title'=>'Если у вас имеются правила мерчендайзинга, загрузите документ или напишите какие у вас имеются правила',
            'placeholder'=>'Мой ответ',
            'type'=>3,
            'key'=>'marketing-9',
        ]);

        Form::create([
            'question_id'=>'59',
            'title'=>'Если у вас имеются стандарты мерчендайзинга, загрузите документ или напишите ваши стандарты',
            'placeholder'=>'Мой ответ',
            'type'=>3,
            'key'=>'marketing-10',
        ]);

        Form::create([
            'question_id'=>'60',
            'title'=>'Кто является целевой аудиторией вашего продукта?',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'marketing-11',
        ]);

        Form::create([
            'question_id'=>'60',
            'title'=>'Возраст',
            'placeholder'=>'Мой ответ',
            'type'=>4,
            'key'=>'marketing-12',
            'answers'=>json_encode([
                "ans-1"=>'До 18',
                "ans-2"=>'18-24',
                "ans-3"=>'25-34',
                "ans-4"=>'35-44',
                "ans-5"=>'45-54',
                "ans-6"=>'55-64',
                "ans-7"=>'От 65'
            ])
        ]);

        Form::create([
            'question_id'=>'60',
            'title'=>'Пол',
            'placeholder'=>'Мой ответ',
            'type'=>2,
            'key'=>'marketing-13',
            'answers'=>json_encode([
                "ans-1"=>'Мужской',
                "ans-2"=>'Женский',
            ])
        ]);

        Form::create([
            'question_id'=>'60',
            'title'=>'Язык',
            'placeholder'=>'Мой ответ',
            'type'=>4,
            'key'=>'marketing-14',
            'answers'=>json_encode([
                "ans-1"=>'Казахский',
                "ans-2"=>'Русский',
                "ans-2"=>'Английский',
                "ans-2"=>'Другое:'
            ])
        ]);

        Form::create([
            'question_id'=>'60',
            'title'=>'Семейное положение',
            'placeholder'=>'Мой ответ',
            'type'=>2,
            'key'=>'marketing-15',
            'answers'=>json_encode([
                "ans-1"=>'Холост / Не замужем',
                "ans-2"=>'Другое:'
            ])
        ]);

        Form::create([
            'question_id'=>'60',
            'title'=>'Сфера деятельности',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'marketing-16',
            'input'=>'textarea'
        ]);

        Form::create([
            'question_id'=>'60',
            'title'=>'Образование',
            'placeholder'=>'Мой ответ',
            'type'=>4,
            'key'=>'marketing-17',
            'answers'=>json_encode([
                "ans-1"=>'Нет образования',
                "ans-2"=>'Школьное',
                "ans-3"=>'Среднее',
                "ans-4"=>'Средне-специальное',
                "ans-5"=>'Высшее',
                "ans-6"=>'Другое:'
            ])
        ]);

        Form::create([
            'question_id'=>'60',
            'title'=>'Описание аудитории',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'marketing-18',
            'input'=>'textarea'
        ]);

        Form::create([
            'question_id'=>'60',
            'title'=>'Поведенческие привычки аудитории',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'marketing-19',
            'input'=>'textarea'
        ]);

        Form::create([
            'question_id'=>'60',
            'title'=>'Кто ваша целевая аудитория среди покупателей франшиз?',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'marketing-20',
            'input'=>'textarea'
        ]);

        
        //Mar

        Form::create([
            'question_id'=>'61',
            'title'=>'Какой продукт вы производите?',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'marketing-21',
            'input'=>'textarea'
        ]);

        Form::create([
            'question_id'=>'61',
            'title'=>'Как происходит производство вашего продукта?',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'marketing-22',
            'input'=>'textarea'
        ]);

        Form::create([
            'question_id'=>'61',
            'title'=>'Какие боли решает ваш продукт?',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'marketing-23',
            'input'=>'textarea'
        ]);

        Form::create([
            'question_id'=>'61',
            'title'=>'Сколько стоит единица вашего продукта?',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'marketing-24',
            'input'=>'number',
        ]);

        Form::create([
            'question_id'=>'61',
            'title'=>'Какое количество продукта в месяц вы продаете?',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'marketing-25',
            'input'=>'number',
        ]);

        


        Form::create([
            'question_id'=>'62',
            'title'=>'Как будет строится ваш маркетинг с франчайзи?',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'marketing-26',
            'input'=>'textarea'
        ]);



        Form::create([
            'question_id'=>'62',
            'title'=>'Какие каналы коммуникации наиболее эффективны для продвижения продукта компании',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'marketing-27',
            'input'=>'textarea'
        ]);

        Form::create([
            'question_id'=>'62',
            'title'=>'Какие показатели конверсии для вас являются стандартными?',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'marketing-28',
            'input'=>'textarea'
        ]);

        Form::create([
            'question_id'=>'62',
            'title'=>'Какие цены за результат маркетинговый являются для вас стандартными?',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'marketing-29',
            'input'=>'textarea'
        ]);

        Form::create([
            'question_id'=>'62',
            'title'=>'Какой у вас рекламный бюджет на 1 месяц?',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'marketing-30',
            'input'=>'textarea'
        ]);

        Form::create([
            'question_id'=>'62',
            'title'=>'Сколько вы планируете тратить на рекламу?',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'marketing-31',
            'input'=>'number'
        ]);

        Form::create([
            'question_id'=>'62',
            'title'=>'Какие рекламные каналы используете?',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'marketing-32',
            'input'=>'textarea'
        ]);

        Form::create([
            'question_id'=>'62',
            'title'=>'Даёт ли франчайзер для работы рекламные материалы франчайзи?',
            'placeholder'=>'Мой ответ',
            'type'=>2,
            'key'=>'marketing-33',
            'answers'=>json_encode([
                "ans-1"=>'Да',
                "ans-2"=>'Неь',
                "ans-3"=>'Другое:',
            ])
        ]);

        Form::create([
            'question_id'=>'62',
            'title'=>'Если ответ да, то какие рекламные материалы и в каком количестве?',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'marketing-34',
            'input'=>'textarea'
        ]);






        Form::create([
            'question_id'=>'63',
            'title'=>'Заказываете ли услуги маркетинговые у поставщиков? Если да, то у каких?',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'marketing-35',
            'input'=>'textarea'
        ]);

        Form::create([
            'question_id'=>'63',
            'title'=>'Сколько вы тратите на работу с поставщиками?',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'marketing-36',
            'input'=>'number',
        ]);

        Form::create([
            'question_id'=>'63',
            'title'=>'Есть ли брендированная продукция? Если да, то нужно ли её закупать франчайзи?',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'marketing-37',
            'input'=>'textarea'
        ]);

        Form::create([
            'question_id'=>'63',
            'title'=>'Где должен франчайзи закупать брендированные продукции?',
            'placeholder'=>'Мой ответ',
            'type'=>1,
            'key'=>'marketing-38',
            'input'=>'textarea'
        ]);

        // 7 
        
        Form::create([
            'question_id'=>'64',
            'title'=>'Добавить логотип компании (Страница 1) (формат: PNG/SVG)',
            'placeholder'=>'Мой ответ',
            'type'=>3,
            'key'=>'marketing-39',
        ]);

        Form::create([
            'question_id'=>'64',
            'title'=>'Загрузить фотографию основателя бизнеса. (Страница 3)',
            'placeholder'=>'Мой ответ',
            'type'=>3,
            'key'=>'marketing-40',
        ]);

        Form::create([
            'question_id'=>'64',
            'title'=>'Загрузите фотографию помещения действующей франшизной точки (Страница 5)',
            'placeholder'=>'Мой ответ',
            'type'=>3,
            'key'=>'marketing-41',
        ]);

        Form::create([
            'question_id'=>'64',
            'title'=>'Загрузите фотографию продукта/услуги компании (Страница 7) ',
            'placeholder'=>'Мой ответ',
            'type'=>3,
            'key'=>'marketing-42',
        ]);


        //EMPTY

        Form::create([
            'question_id'=>'68',
            'title'=>'',
            'placeholder'=>'',
            'type'=>1,
            'key'=>'marketing-43',
        ]);

        Form::create([
            'question_id'=>'68',
            'title'=>'',
            'placeholder'=>'',
            'type'=>1,
            'key'=>'marketing-44',
        ]);

        Form::create([
            'question_id'=>'68',
            'title'=>'',
            'placeholder'=>'',
            'type'=>1,
            'key'=>'marketing-45',
        ]);

        Form::create([
            'question_id'=>'68',
            'title'=>'',
            'placeholder'=>'',
            'type'=>1,
            'key'=>'marketing-46',
        ]);

        Form::create([
            'question_id'=>'68',
            'title'=>'',
            'placeholder'=>'',
            'type'=>1,
            'key'=>'marketing-47',
        ]);
    
    }
}
