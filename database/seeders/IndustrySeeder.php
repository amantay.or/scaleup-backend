<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Industry;

class IndustrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Industry::create(['name'=>'IT']);
        Industry::create(['name'=>'Cooking']);
        Industry::create(['name'=>'Accountant']);
        
    }
}
