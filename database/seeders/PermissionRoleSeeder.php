<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
class PermissionRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //ADMIN
        Role::find(8)->givePermissionTo('role-edit');//Admin - role-edit
        Role::find(8)->givePermissionTo('service-edit');//Admin - service-edit
        Role::find(8)->givePermissionTo('top-faq-edit');//Admin - top-faq-edit
        Role::find(8)->givePermissionTo('faq-category-edit');//Admin - faq-category-edit
        Role::find(8)->givePermissionTo('faq-edit');//Admin - faq-edit
        Role::find(8)->givePermissionTo('payment-control');//Admin - payment-control
        Role::find(8)->givePermissionTo('worker');//Admin - worker

        //MODERATOR
        Role::find(6)->givePermissionTo('payment-control');//Moderator - payment-control

        //SALE_DEPARTMENT
        Role::find(7)->givePermissionTo('payment-control');//Sale department - payment-control

        //CLIENT
        Role::find(1)->givePermissionTo('accepted');//Client - Accepted
        
    }
}
