<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Faq;

class FaqSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Faq::create([
            'title'=>'1',
            'text'=>'1',
            'faq_category_id'=>1
            ]);
        Faq::create([
            'title'=>'2',
            'text'=>'2',
            'faq_category_id'=>1
            ]);
        Faq::create([
            'title'=>'2',
            'text'=>'2',
            'faq_category_id'=>1
            ]);
    }
}
