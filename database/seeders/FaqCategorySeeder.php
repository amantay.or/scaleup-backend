<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\FaqCategory;

class FaqCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FaqCategory::create(['title'=>'Секция 1']);
        FaqCategory::create(['title'=>'Секция 2']);
        FaqCategory::create(['title'=>'Секция 3']);
    }
}
