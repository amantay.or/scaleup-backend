<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Section;

class SectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Section::create([
            'name'=>'Стратегический раздел',
            'description'=>'Learn from a growing library of 1,982 websites and 3,829 component examples. Easily filterable to find the inspiration you need, quickly.',
            'link'=>"https://www.youtube.com/embed/nI_-8NhhrqE",
           ]);
        Section::create([
            'name'=>'Финансовый раздел',
            'description'=>'Learn from a growing library of 1,982 websites and 3,829 component examples. Easily filterable to find the inspiration you need, quickly.',
            'link'=>"https://www.youtube.com/embed/nI_-8NhhrqE",
            ]);
        Section::create([
            'name'=>'Юридический раздел',
            'description'=>'Learn from a growing library of 1,982 websites and 3,829 component examples. Easily filterable to find the inspiration you need, quickly.',
            'link'=>"https://www.youtube.com/embed/nI_-8NhhrqE",
            ]);
        Section::create([
            'name'=>'Маркетинговый раздел',
            'description'=>'Learn from a growing library of 1,982 websites and 3,829 component examples. Easily filterable to find the inspiration you need, quickly.',
            'link'=>"https://www.youtube.com/embed/nI_-8NhhrqE",
            ]);
    }
}
