<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\PaymentType;

class PaymentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PaymentType::create(['name'=>'Онлайн платежи']);
        PaymentType::create(['name'=>'Счет на оплату']);
        PaymentType::create(['name'=>'Не выбрано']);
    }
}
