<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Payment;

class PaymentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Payment::create(['user_id'=>8]);
        Payment::create(['user_id'=>9]);
        Payment::create(['user_id'=>10]);
    }
}
