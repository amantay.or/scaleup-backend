<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            
            RoleSeeder::class,
            PermissionSeeder::class,
            PermissionRoleSeeder::class,
            UserSeeder::class,
            IndustrySeeder::class,
            ServiceSeeder::class,
            TopFaqSeeder::class,
            BlockSeeder::class,
            FaqCategorySeeder::class,
            FaqSeeder::class,
            PaymentStatusSeeder::class,
            PaymentTypeSeeder::class,
            SectionSeeder::class,
            QuestionSeeder::class,
            LegalFormSeeder::class,
            StrategyFormSeeder::class,
            FinancialFormSeeder::class,
            MarketingFormSeeder::class,
            PaymentSeeder::class
        ]);
    }
}
