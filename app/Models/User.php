<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use App\Notifications\ResetPassword as ResetPasswordNotification;
use App\Notifications\VerifyNotification;
use App\Notifications\WorkerCreate as WorkerCreateNotification;
use Spatie\Permission\Models\Role;
use Log;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens,HasFactory, Notifiable,HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fio',
        'phone',
        'company',
        'email',
        'password',
        'email_verified_at',
        'avatar',
        'docs'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $guard_name = 'api';

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyNotification());
    }


    public function payment()
    {
        return $this->hasOne('App\Models\Payment')->with(['service','paymentStatus','paymentType'])->latest();
    }

    public function onlyPayment()
    {
        return $this->hasOne('App\Models\Payment')->latest();
    }

    public function answers()
    {
        return $this->hasMany('App\Models\Answer');
    }

    public function production()
    {
        if(auth()->user()->roles[0]->name == 'client')
            return $this->hasOne('App\Models\Production')->withTrashed();
        else 
            return $this->hasOne('App\Models\Production');

    }

    public function industry()
    {
        return $this->belongsTo('App\Models\Industry');
    }

    public function public_document()
    {
        return $this->hasOne('App\Models\PublicDocument')->latest();
    }

    public function document()
    {
        return $this->hasOne('App\Models\Document');
    }

    public function worksheet()
    {
        return $this->hasOne('App\Models\Worksheet');
    }

    public function commentary()
    {
        return $this->hasOne('App\Models\Commentary');
    }

    public function updateModel($request)
    {
        return tap(auth()->user())->update($request);
    }

    public function updatePassword($request)
    {
        $request['password'] = bcrypt($request['password']);
        
        return tap(auth()->user())->update($request);
    }

    public function workers()
    {
        $workers = $this->whereHas('roles',function($q){
            $q->whereBetween('id',[2,7]);
        })->get();

        return $workers;
    }

    public function createWorker($request,$password){

        $user = $this->create($request);

        $user->assignRole(Role::find($request['role_id']));

        $user->notify(new WorkerCreateNotification($user,$password));

        return $user;

    }

    public function deleteWorker($user)
    {
        $this->roles()->detach(); 

        $user->delete();

        return response()->json([
            "status"=>"success"
        ], 204);
    }

    public function updateWorker($request,$user){

        $user->syncRoles(Role::find($request['role_id']));

        $user = tap($user)->update($request);

        $user->notify(new WorkerCreateNotification($user,$request['send_passsword']));

        return $user;

    }

    public function list()
    {
        $users = $this->whereHas('roles',function($q){
            $q->where('id',1);
        })->with('industry')->get();

        return $users;
    }

    public function updateUser($request,$user)
    {
        return tap($user)->update($request);

    }


}
