<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use File;
use App\Models\User;
use App\Models\Form;
use App\Models\Answer;
use Log;
use Response;
use App\Models\PublicDocument;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class FileModel extends Model
{
    use HasFactory;
    
    protected $forms;
    protected $forms_financial;

    public function createUserFiles($id)
    {
        $user = User::find($id);
        request()->user = $user;

        $this->forms = Form::with('answer')->get()->keyBy('key');
        $this->forms_financial = Form::with('answer')->where('key','like','%financial-%')->get()->keyBy('key');
       
        return $this->createUserFolders($id);
    }

    public function createIndustry($id){
        $user = User::find($id);
        request()->user = $user;

        $this->forms = Form::with('answer')->get()->keyBy('key');

        $this->setIndustry($user);
    }

    public function setIndustry($user){

        
        if($this->forms['legal-2']->answer){

            $ans = $this->forms['legal-2']->answer->answers;
            if($ans) {
                foreach($ans as $key=>$value)
                {
                    $answer = $key;
                }
                $user->industry_id = $answer;
                $user->save();
            }   
        }
    }

    public function getUserFiles(User $user)
    {
        $auth = auth()->user();
        if($auth->roles[0]->name == "client"){
            if($auth->docs == 0){
                return response()->json(['status'=>'error','message'=>'Ваши документы еще не готовы!'],404); 
            }
            if($user->id != $auth->id){
                return response()->json(['status'=>'error','message'=>'Вы не можете просматривать чужие документы!'],403); 
            }
        }
        
        $path = 'documents/users/'.$user->id;
        
        $data = [];
        
        $data =  $this->recoursive($path);
        
        $role = auth()->user()->roles[0]->name;
        
        if($role == "financier" ){
            unset($data['law']);
            unset($data['marketing']);
            unset($data['strategy']);
        }
        else if($role == "lawyer"){
            unset($data['finance']);
            unset($data['marketing']);
            unset($data['strategy']);
        }  
        else if($role == "marketer"){
            unset($data['law']);
            unset($data['finance']);
        }     
     
        return $data;

    }

    public function recoursive($path)
    {
        $docs = File::files(storage_path('app/'.$path));

        $directories = Storage::directories($path);
        $data = [];
        foreach ($docs as $key => $doc) {

            $data["file_".$key] = ['name'=>$doc->getFilename(),'size'=>(int)($doc->getSize()/1024),'type'=>'file'];
        }

        
       
        foreach ($directories as $key => $dir) {

           
            $dir_arr = explode("/",$dir);
            $dir_name = $dir_arr[array_key_last($dir_arr)];

            
            $new_path = $path.'/'.$dir_name;
            $docs = File::files(storage_path('app/'.$new_path));

            $file_count = count(File::allFiles(storage_path('app/'.$new_path)));
            
            $directories = Storage::directories($new_path);
           
            if(!empty($docs) || !empty($directories)){
              
                $data[$dir_name] =  ['data'=>$this->recoursive($new_path),'count'=>$file_count,'type'=>'folder'];
            }
            else{
                $data[$dir_name] = ['data'=>null,'count'=>$file_count,'type'=>'folder'];
            }

        }

        return $data;
       
    }

    public function getFileByLink(Request $request, User $user)
    {
       
        $path = storage_path().'/app/documents/users/'.$user->id.'/'.$request->link;
        
      
        return Response::download($path);
    }
    public function getGoogleFileByLinkPublic(Request $request, $token){

        $section = explode("/",$request->link)[0];

        $public_document = PublicDocument::where('token',$token)->first();

        if(!$public_document){
            return response()->json(['status'=>'error',"message"=>'Документы не нашлись'],404); 
        }
        $user = $public_document->user;


        $path = explode("/",$user->id.'/'.$request->link);
        
        $file_path = "/1SfQBBNzmcUrPPssDe4TBpM1c6RJUbORw";

        
        foreach ($path as $key => $folder) {

            if($key +1 == count($path)){
                $folder = explode(".",$folder)[0];
            }
            $files = collect(Storage::disk('google')->listContents($file_path, false));
      
            $file = $files->where('filename', '=', $folder)->first();

            $file_path = $file['path'];
            
        }

        $rawData = Storage::disk('google')->get($file_path);


       
        return response($rawData, 200)
            ->header('ContentType', $file['mimetype'])
            ->header('Content-Disposition', "attachment; filename=".$file['name']);

        return $rawData;
    }

    public function getGoogleFileByLink(Request $request, User $user)
    {
        
        $section = explode("/",$request->link)[0];

        $path = explode("/",$user->id.'/'.$request->link);
        
        $file_path = "/1SfQBBNzmcUrPPssDe4TBpM1c6RJUbORw";

        
        foreach ($path as $key => $folder) {

            if($key +1 == count($path)){
                $folder = explode(".",$folder)[0];
            }
            $files = collect(Storage::disk('google')->listContents($file_path, false));
      
            $file = $files->where('filename', '=', $folder)->first();

            $file_path = $file['path'];
            
        }

        $rawData = Storage::disk('google')->get($file_path);


       
        return response($rawData, 200)
            ->header('ContentType', $file['mimetype'])
            ->header('Content-Disposition', "attachment; filename=".$file['name']);

        return $rawData;
    }

    public function openGoogleFileByLink(Request $request, User $user){
        $section = explode("/",$request->link)[0];

        $path = explode("/",$user->id.'/'.$request->link);
        
        $file_path = "/1SfQBBNzmcUrPPssDe4TBpM1c6RJUbORw";

        
        foreach ($path as $key => $folder) {

            if($key +1 == count($path)){
                $folder = explode(".",$folder)[0];
            }
            $files = collect(Storage::disk('google')->listContents($file_path, false));
      
            $file = $files->where('filename', '=', $folder)->first();

            $file_path = $file['path'];
            
        }

        return response()->json(['status'=>'success','link'=>$file['basename']]);
    
    }

   


    public function createUserFolders($username)
    {
        $service = request()->user->payment->service_id;
      
        $path = 'documents/users/'.$username;
        $template = "documents/templates-files$service";
       
        Storage::deleteDirectory($path);
        Storage::disk('local')->makeDirectory($path);
        File::copyDirectory(storage_path('app/'.$template), storage_path('app/'.$path));
        
        return $this->getDocs($username);

    }

    public function getDocs($username)
    {
        $service = request()->user->payment->service_id;
        $path = "documents/templates$service";
       
        $docs = File::allFiles(storage_path('app/'.$path));

        
       
        foreach ($docs as $key => $document) {

            try {
                $doc = $document->getRelativePathname();
                $ext = pathinfo($doc, PATHINFO_EXTENSION);
                
                if($ext!="docx"){

                    $path = 'documents/users/'.$username.'/'.$doc;

                    Storage::copy("documents/templates$service/".$doc, $path);
                    
                    if($ext == "pptx"){
                        $this->workWithSlide($username,$doc,$document);
                    }
                    if($ext == "xlsx"){
                    
                        $this->workWithExcel("documents/templates$service/".$doc,$username,$document);
                    }
                    continue;
                }
                $path = "documents/templates$service/".$doc;

                $phpword = new \PhpOffice\PhpWord\TemplateProcessor(storage_path('app/'.$path));
                    
                $phpword = $this->fillDocuments($phpword,'legal',1,129);
                $phpword = $this->fillDocuments($phpword,'strategy',1,97);
                $phpword = $this->fillDocuments($phpword,'financial',1,46);
                $phpword = $this->fillDocuments($phpword,'marketing',1,47);

                $path = 'documents/users/'.$username.'/'.$doc;

                $phpword->saveAs(storage_path('app/'.$path));

            } catch (\Throwable $th) {
                //throw $th;
            }
        }

       

        return response()->json(['status'=>'success','message'=>'OK']);
    }

    public function fillDocuments($phpword,$section,$start,$finish){

        for ($i=$start; $i <= $finish; $i++) { 

            $key = $section.'-'.$i;
            if($this->forms[$key]->answer){
                if($this->forms[$key]->type == 1 || $this->forms[$key]->type == 2)  
				{
					$ans = $this->forms[$key]->answer->answers;
					if(!$ans)
                        $ans = "";
                    else{
                        foreach($ans as $key2=>$value){
                            $answer = $value;
                        }
                        $ans = $answer;
                    }
			
                    $phpword->setValue($key, $ans);
				}
                else if($this->forms[$key]->type == 4){

                    $answers = $this->forms[$key]->answer->answers;
                    
					if(!$answers)
                        $answers = [];
                    
                    $answers = $this->getValuesOfCheckbox($answers);

                    $phpword->setValue($key, $answers);
                }
                else{

                   
                    $answers = $this->forms[$key]->answer->answers;
                    if(!$answers){
                        $phpword->setValue($key, ' ');
                    }
                    else{

                        $answers = $answers[0];

                        $format = explode(".",$answers)[1];
                        if($format != "png" && $format != "jpg" && $format != "jpeg"){
                            continue;
                        }
                        $image_path = Storage::path($answers);
                        
                        $phpword->setImageValue($key, array(
                            'src'  => $image_path,
                            'size' => array( 5000,5000 ) //px
                        ));
                    }
                }
            }
            else {
                $phpword->setValue($key, ' ');
            }   
        }
        return $phpword;
    }

    public function getValuesOfCheckbox($answers){

        $ans = "";
        foreach ($answers as $key => $answer) {
          
            $ans.= "- ".$answer;
            $ans.="</w:t><w:br/><w:t>";
        }
        return $ans;
    }

    public function uploadFileToUser(Request $request, User $user){

        $document = $user->document;
        
        $path = 'documents/users/'.$user->id.'/'.$request['link'].'/';

        $path_to_google_disk = $user->id.'/'.$request['link'];
        
        if($document){
          
            $start2 = microtime(true);

            $google_path = "/1SfQBBNzmcUrPPssDe4TBpM1c6RJUbORw";

            $directories = explode("/",$path_to_google_disk);

            foreach ($directories as $key => $dir) {
                $contents = collect(Storage::disk('google')->listContents($google_path, false));
            
                $folder = $contents->where('filename', '=', $dir)->first();

                $google_path = $folder['path'];
            }
            Storage::disk('google')->put($google_path.'/'.$request['file']->getClientOriginalName(),file_get_contents($request['file']));
        }
        
        $request['file']->storeAs($path,$request['file']->getClientOriginalName());

        return $this->getUserFiles($user);

    }

    public function deleteFileToUser(Request $request, User $user){

        $path = storage_path().'/app/documents/users/'.$user->id.'/'.$request['link'];

        if(File::exists($path)){
           
            unlink($path);
        }

        return $this->getUserFiles($user);
    }

    public function workWithExcel($doc, $username, $document) {
    
        $temp_path = storage_path('app/'.$doc);
        $path = storage_path('app/documents/users/'.$username.'/'.$document->getRelativePathName());
        
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($path);
        foreach ($this->forms_financial as $key => $form) {
            $key = $form->key;
            
            if($this->forms[$key]->answer){
                
                if($this->forms[$key]->type == 1 || $this->forms[$key]->type == 2)  
				{
					$ans = $this->forms[$key]->answer->answers;
					if(!$ans)
                        $ans = "";
                    else{
                        foreach($ans as $key2=>$value){
                            $answer = $value;
                        }
                        $ans = $answer;
                    }
                    $k = '${'.$key.'}';     
                    foreach ($spreadsheet->getWorksheetIterator() as $worksheet) {
                        $ws = $worksheet->getTitle();
                        foreach ($worksheet->getRowIterator() as $row) {
                            $cellIterator = $row->getCellIterator();
                            $cellIterator->setIterateOnlyExistingCells(true);
                            foreach ($cellIterator as $cell) {
                                if (strpos($cell->getValue(),$k) !== false) {
                                    $cell_value = str_replace($k,$ans,$cell->getValue());
                                    $worksheet->setCellValue($cell->getCoordinate(), $cell_value);
                                }
                            }
                        }
                    }

				}
            }
            else {
                $k = '${'.$key.'}';
                foreach ($spreadsheet->getWorksheetIterator() as $worksheet) {
                    $ws = $worksheet->getTitle();
                    foreach ($worksheet->getRowIterator() as $row) {
                        $cellIterator = $row->getCellIterator();
                        $cellIterator->setIterateOnlyExistingCells(true);
                        foreach ($cellIterator as $cell) {
                            if (strpos($cell->getValue(),$k) !== false) {
                                $cell_value = str_replace($k,'',$cell->getValue());
                                $worksheet->setCellValue($cell->getCoordinate(), $cell_value);
                            }
                        }
                    }
                }

            }   
        } 
        $writer = new Xlsx($spreadsheet);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="invoice.xlsx"');
        ob_start();
        $writer->save('php://output');
        $content = ob_get_contents();
        ob_end_clean();
        
        File::put($path, $content,0755); 
        
            
    }

    public function workWithSlide($username, $doc, $document) {

        $path = storage_path('app/documents/users/'.$username.'/'.$document->getRelativePathName());
        $pathTo = storage_path('app/documents/users/'.$username.'/'.$document->getRelativePath());
        $zip = new \ZipArchive;
        $res = $zip->open($path);
        $zip->extractTo($pathTo.'/check');
        $zip->close();

        $filesInFolder = File::files($pathTo.'/check/ppt/slides');   
        $files = [];  
        foreach($filesInFolder as $path) { 
            $files[] = pathinfo($path)['filename'].".xml";
        } 

        $this->renderTextSlide($files,$pathTo);

        $this->renderSlide($pathTo,$document->getFileName());
    }

    public function renderTextSlide($files,$path){
      
       
        foreach ($files as $file) {
            $slide = $path.'/check/ppt/slides/'.$file;
            $file_contents = file_get_contents($slide);
            $file_contents = $this->fillSlide($file_contents,$path,$file);
            file_put_contents($slide,$file_contents);
        }
        
    }

    public function fillSlide($file_contents,$path,$file){
        
        foreach ($this->forms as $key => $form) {
            $key = $form->key;

           
            if($this->forms[$key]->answer){
                if($this->forms[$key]->type == 1 || $this->forms[$key]->type == 2)  
				{
					$ans = $this->forms[$key]->answer->answers;
					if(!$ans)
                        $ans = "";
                    else{
                        foreach($ans as $key2=>$value){
                            $answer = $value;
                        }
                        $ans = $answer;
                    }
                    $file_contents = str_replace('${'.$key."}",$ans,$file_contents);
				}
                else if($this->forms[$key]->type == 4){

                    $answers = $this->forms[$key]->answer->answers;
                    
					if(!$answers)
                        $answers = [];
                    
                    $answers = $this->getValuesOfCheckbox($answers);

                    $file_contents = str_replace('${'.$key."}",$ans,$file_contents);
                }
                else{

                    $key_pos = strpos($file_contents, $key);
                    if($key_pos === false) {
                        continue;
                    }
                    $new_contents = substr($file_contents,$key_pos);
                    
                    $new_contents = substr($new_contents,strpos($new_contents, "r:embed="));
                    $new_contents = explode("><a:extLst/></a:blip>",$new_contents)[0];

                    $id = substr($new_contents,9,-1);
                    $slide = $path.'/check/ppt/slides/_rels/'.$file.'.rels';

                    $rel_contents = file_get_contents($slide);
                    $rel_contents_2 = substr($rel_contents,strpos($rel_contents, $id));
                    
                    $start = strpos($rel_contents_2, "../media/");
                    $finish = strpos($rel_contents_2, "\"/></Relationships>");
                    
                    $answers = $this->forms[$key]->answer->answers;
                    if(!$answers){
                        $phpword->setValue($key, ' ');
                    }
                    else{

                        $answers = $answers[0];

                        $format = explode(".",$answers)[1];
                        if($format != "png" && $format != "jpg" && $format != "jpeg"){
                            continue;
                        }
                        $name = explode("/",$answers);
                        $name = $name[count($name) - 1];
                        
                        
                        $data = explode($id,$rel_contents)[0].explode("../media",$rel_contents_2)[0]. "../media/".$name.substr($rel_contents_2,$finish);
                        file_put_contents($slide,$data);
                        
                        
                        if (!Storage::exists(explode("app/",$path)[1].'/check/ppt/media/'.$name)) {
                            $data = Storage::copy($answers, explode("app/",$path)[1].'/check/ppt/media/'.$name);
                            
                        }
                    }
                    
                }
            }
            else {
                $file_contents = str_replace('${'.$key."}",'',$file_contents);
            }   
        }
        return $file_contents;
    }
    
    public function renderSlide($path, $filename){

        
        // Initialize archive object
        $zip = new \ZipArchive();
        $zip->open($path.'/file.zip', \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

        // Create recursive directory iterator
        /** @var SplFileInfo[] $files */
        $files = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator($path.'/check'),
            \RecursiveIteratorIterator::LEAVES_ONLY
        );

        foreach ($files as $name => $file)
        {
            // Skip directories (they would be added automatically)
            if (!$file->isDir())
            {
                // Get real and relative path for current file
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($path.'/check') + 1);

                // Add current file to archive
                $zip->addFile($filePath, $relativePath);
            }
        }

        // Zip archive will be created only after closing object
        $zip->close();

        rename($path.'/file.zip',$path."/".$filename);

        Storage::deleteDirectory(explode('app/',$path)[1].'/'.'check');
    }

}

