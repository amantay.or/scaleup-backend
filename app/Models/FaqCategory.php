<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FaqCategory extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
    ];

    public function faqs()
    {
        return $this->hasMany('App\Models\Faq');
    }

    public function createModel($request)
    {
        return $this->create($request);
    }

    public function updateModel($request)
    {
        
        foreach ($request['categories'] as $key => $category) {
           
            $this->where('id',$category['id'])->update(['title'=>$category['title']]);
        }
        
    }
}
