<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'price',
        'work',
        'data',
    ];

    public function updateModel($request,$service)
    {
        return $service->update($request);
    }

    public function users(){
        return $this->belongsToMany('App\Models\User','payments');
    }
}
