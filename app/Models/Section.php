<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Worksheet;

class Section extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'link', 
        'questions'
    ];

    public function questions()
    {
        return $this->hasMany('App\Models\Question');
    }

    public function answers()
    {
        if(isset(request()->user->id)){
            $id = request()->user->id;
        }
            
        else
            $id = auth()->user()->id;

        return $this->hasManyThrough('App\Models\Form','App\Models\Question')->whereHas('answer', function($q){
            $q->whereNotNull('answers');
         });
    }

    public function forms()
    {
        return $this->hasManyThrough('App\Models\Form','App\Models\Question');
    }

    public function worksheet()
    {
        return auth()->user()->worksheet();
    }


    public function updateModel($request,$section)
    {
       return tap($section)->update($request);
    }
}
