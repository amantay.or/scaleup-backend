<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Http\Resources\Payment\PaymentResourceWithInvoice;
use App\Http\Resources\Payment\PaymentResource;
use Illuminate\Support\Facades\Storage;
use Log;
use App\Models\Production;
use App\Models\Notification;
use App\Models\User;
use App\Models\PaymentStatus;
use DB;

class Payment extends Model
{
    use HasFactory;

    const ONLINE = 1;

    const INVOCE = 2;

    const CREATED_ST = 1;
    
    const PENDING_ST = 2;

    const ACCEPTED_ST = 3;

    protected $fillable = [
        'payment_type_id',
        'service_id',
        'payment_status_id',
        'user_id',
        'link'
    ];

    public function paymentStatus()
    {
        return $this->belongsTo('App\Models\PaymentStatus');
    }

    public function paymentType()
    {
        return $this->belongsTo('App\Models\PaymentType');
    }

    public function service()
    {
        return $this->belongsTo('App\Models\Service');
    }

    public function invoice()
    {
        return $this->hasOne('App\Models\Invoice')->latest();
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function storeModel($request)
    {
       
        return $this->updateOrCreate(
            ['user_id'=>$request['user_id']],$request);
    }

    public function getPayment()
    {
        $payment = Auth::user()->payment;
        return new PaymentResource($payment);
    }

    public function created_st()
    {
        return $this->payment_status_id == Payment::CREATED_ST;
    }

    public function storeInvoice($request)
    {
        $fileName = $this->storeFile($request['link']);
     
        $payment = Auth::user()->payment;
        $payment->invoice()->create(['link'=>$fileName]);

        if($payment->created_st()){
            $payment->changeStatus(Payment::PENDING_ST);
           
        }
        $company = Auth::user()->company;

        $users = User::whereHas("roles", function($q){ $q->where("name", "sale_department")->orWhere('name','admin'); })->get();
        foreach ($users as $key => $user) {
            Notification::notify("$company ждет подтверждения оплаты",$user);
        }

        return new PaymentResource($payment);
    }

    public function storeBank()
    {
        $payment = auth()->user()->onlyPayment;

        $payment = $this->create($payment->toArray());

        if($payment->checkBank()) {
            return response()->json(['status'=>'error','message' => 'Already Paid!'], 404);
        }
       
        try {
            $client = new \GuzzleHttp\Client();
            $endpoint = env("IOKA_LINK")."/api/payments/register/";
            $response = $client->request('POST', $endpoint, [
                'headers' => [
                    'Authorization' => 'Bearer '.env("IOKA_SECRET_KEY"),
                    'Content-type' => 'application/json; charset=utf-8',
                    'Accept' => 'application/json;'
                ],
                'body' => json_encode(
                    [
                        "order_id" => $payment->id,
                        "amount" => $payment->service->price,
                        "back_url" => env("FRONT_URL_LINK")
                    ]
                )
            ]);

            $statusCode = $response->getStatusCode();
            $content = $response->getBody();
            
            
            $ref = json_decode($content, true)['reference'];
            $link = json_decode($content, true)['url'];
            $payment->link = $ref;
            $payment->payment_status_id = 1;
            $payment->payment_type_id = 1;
            $payment->save();

            return response()->json(['status'=>'success','link'=>$link]);
        } catch (\Throwable $th) {
            return response()->json(['status'=>'error','message' => 'Not Found!'], 404);
        }
    }

    public function checkBank()
    {
        if($this->payment_status_id == 3) {
            return 1;
        }
        if(isset($this->link) && $this->payment_status_id != 3) {
            try {
                $client = new \GuzzleHttp\Client();
                $endpoint = env("IOKA_LINK")."/api/payments/".$this->link.'/'.'status/';
                $response = $client->request('GET', $endpoint, [
                    'headers' => [
                        'Authorization' => 'Bearer '.env("IOKA_SECRET_KEY"),
                        'Content-type' => 'application/json; charset=utf-8',
                        'Accept' => 'application/json;'
                    ]
                ]);
    
                $statusCode = $response->getStatusCode();
                $content = $response->getBody();
                
                $code = json_decode($content, true)['error_code'];
    
                if($code == 0) {
                    $this->payment_status_id = 3;
                    $this->save();

                    Worksheet::firstOrCreate(['user_id'=>auth()->user()->id]);
                    Notification::notify("Анкеты доступны к заполнению",auth()->user());
                    return 1;
                }
                else {
                    $this->payment_status_id = 4;
                    $this->save();
                    
                }
                
            } catch (\Throwable $th) {
            }
        }
        return 0;
    }

    private function changeStatus($status)
    {
        $this->payment_status_id = $status;
        $this->save();
        return $this;
    }

    private function storeFile($file)
    {
        
        $fileName = time().'-'.uniqid().'.'.$file->extension();

      

        $path = $file->storeAs('public/invoices',$fileName);
       
      //  Storage::put('invoices/'.$fileName, $file);
            
        return "invoices/".$fileName;
    }

    public function accepted($user)
    {
        $payment = $user->payment;
    
        if($payment->payment_status_id == Payment::PENDING_ST){

            $payment = $payment->changeStatus(Payment::ACCEPTED_ST);
            
           
            Worksheet::firstOrCreate(['user_id'=>$user->id]);

            
            Notification::notify("Анкеты доступны к заполнению",$user);

        }
        
    
        return new PaymentResourceWithInvoice($payment);
    }
    
    public function selectPayment($request)
    {
        return tap(Auth::user()->payment)->update($request);
    }

    public function lastPayments() {
        return $this->hasMany('App\Models\Payment')
             ->select('*')
             ;
    }

    public function list()
    {
        $payments = Payment::join(DB::raw('(SELECT max(id) id FROM payments group by user_id) t'), function($join){
                $join->on('payments.id', '=','t.id');
            })->get();

        return $payments;
    }

    public function show($user)
    {
        return  $user->payment;
    }
    
}
