<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TopFaq extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'text',
    ];

    public function updateModel($request,$topFaq)
    {
        return tap($topFaq)->update($request);
    }
}
