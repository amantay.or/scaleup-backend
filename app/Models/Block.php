<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Block extends Model
{
    use HasFactory;

    protected $fillable = [
        'greeting',
        'image',
        'description',
        'link'
    ];

    public function updateModel($request)
    {
        $block = $this->first();

        if(!$request['changed']){
            unset($request['image']);
            unset($request['changed']);
            
            $this->first()->update($request);

            return $this->first();
        }

        if(isset($request['image']))
            $imageName = $this->storeImage($request['image']);

        $request['image'] = $imageName;

        $this->first()->update($request);

        return $this->first();
    }

    private function storeImage($image)
    {
        $imageName = 'block-image'.'.'.$image->extension();
            
        $image->move(public_path('images/block/'), $imageName);
            
        return "images/block/".$imageName;
    }
}
