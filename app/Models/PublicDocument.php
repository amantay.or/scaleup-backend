<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PublicDocument extends Model
{
    use HasFactory;

    protected $fillable = ['strategy','financial','legal','marketing','user_id','token'];

    public function user(){
        return $this->belongsTo('App\Models\User');
    }
}
