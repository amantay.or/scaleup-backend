<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    use HasFactory;

    protected $fillable = ['title','placeholder','answers','key'];

    public function answer()
    {
       
        if(isset(request()->user->id)){
            $id = request()->user->id;
        }
            
        else{
            $id = auth()->user()->id;
        }
        

        return $this->hasOne('App\Models\Answer')->where('user_id',$id);
    }

    protected $attributes = ['disabled'=>true]; // remove on seeds

    protected $casts = [
        'answers' => 'array',
    ];

    public function getAnswersAttribute($value) {
    
        return json_decode(json_decode($value));
    }

    
}
