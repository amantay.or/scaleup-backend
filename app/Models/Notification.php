<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Notifications\NotiNotification;

class Notification extends Model
{
    use HasFactory;

    protected $fillable = [
        'message',
        'type',
        'from',
        'to',
        'status'
    ];

    public static function notify($message,$to,$from=null){

        $data = [
            'message'=>$message,
            'type'=>$to->roles[0]->id,
            'to'=>$to->id,
        ];
        if($from){
            $data['from'] = $from->id;
        }
        self::create($data);

        $to->notify(new NotiNotification($data));
    }

    public function from(){
        return $this->belongsTo('App\Models\User','from');
    }
}
