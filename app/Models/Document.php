<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Storage;
use App\Models\User;
use File;
use Log;
use DB;
// Microsoft

use Microsoft\Graph\Graph;
use League\Flysystem\Filesystem;
use NicolasBeauvais\FlysystemOneDrive\OneDriveAdapter;

class Document extends Model
{
    use HasFactory;

    protected $fillable = [
        'r_strategy',
        'r_legal',
        'r_marketing',
        'r_financial',
        'strategy',
        'financial',
        'legal',
        'marketing',
        'status',
        'user_id'
    ];

    private $filesystem;

    public function __construct(){

        $graph = new Graph();

        $onedrive = DB::table('onedrive')->first();

        $graph->setAccessToken($onedrive->access_token);

        $adapter = new OneDriveAdapter($graph, 'root');

        $this->filesystem = new Filesystem($adapter);

    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function storeGoogleDisk(){

        $user = $this->user;
        $username = $user->id;
        
        $this->createFromLocalOneDrive($username);
    }

    public function createFromLocal($username)
    {   


        $docs = File::allFiles(storage_path('app/documents/users/'.$username));
        
        $user = $this->getUser($username);


        $this->addPermission($user);


        $road = [$username=>$user['path']];

        $road_name = $user['name'];

        Storage::disk('google')->makeDirectory($user['path'].'/'.'marketing');
        Storage::disk('google')->makeDirectory($user['path'].'/'.'law');
        Storage::disk('google')->makeDirectory($user['path'].'/'.'finance');
        Storage::disk('google')->makeDirectory($user['path'].'/'.'strategy');
        
        foreach ($docs as $key => $doc) {
          
            $road = $this->createFileInDrive($doc,$road,$road_name,$key);
        }   
    
    }

    public function addPermission($userGoogle){

        $service = Storage::disk('google')->getAdapter()->getService();
        $permission = new \Google_Service_Drive_Permission();
        $permission->setRole('writer');
        $permission->setType('user');

        $users = User::whereHas('roles',function($q){
            $q->whereIn('name',['editor','financier','lawyer','marketer','moderator','admin']);
        })->get();

        foreach ($users as $key => $user) {
            $permission->setEmailAddress($user->email);
            $permissions = $service->permissions->create($userGoogle['basename'], $permission);
        }
       
       
        

    }

    public function createFileInDrive($doc,$road,$road_name,$k)
    {
       
        $path = $doc->getRelativePath();

        $pathes = explode("/",$path);
        
        foreach ($pathes as $key => $path) {
        
            $road_name_old = $road_name;

            $road_name_new = $road_name."/".$path;
            
            $next_folder = null;
            if(isset($road[$road_name_new])){
                $next_folder = $road[$road_name_new];
                $road_name = $road_name_new;
            }

            if(!$next_folder){
                
                Storage::disk('google')->makeDirectory($road[$road_name_old].'/'.$path);
                
                $s = microtime(true);
                
                $data = collect(Storage::disk('google')->listContents($road[$road_name_old], false));

                $next_folder = $data->where('filename', '=', $path)->first();

                $road[$road_name_new] = $next_folder['path'];

                $road_name = $road_name_new;
            }
        }
       
        
        Storage::disk('google')->put($road[$road_name].'/'.$doc->getFileName(), File::get($doc->getPathName()));
        
        return $road;
    }

    public function getUser($username)
    {
        $users_path = "/1SfQBBNzmcUrPPssDe4TBpM1c6RJUbORw";

        $users = collect(Storage::disk('google')->listContents($users_path, false));
        $user = $users->where('filename', '=', $username)->first();

        if($user)
            Storage::disk('google')->deleteDirectory($users_path.'/'.$user['path']);

        Storage::disk('google')->makeDirectory($users_path.'/'.$username);

        $users = collect(Storage::disk('google')->listContents($users_path, false));
        $user = $users->where('filename', '=', $username)->first();

        return $user;
    }


    //oneDrive


    public function createFromLocalOneDrive($username)
    {   


        $docs = File::allFiles(storage_path('app/documents/users/'.$username));
        
        $this->getUserOneDrive($username);

       
        $road = ["ScaleUp/".$username => "ScaleUp/".$username];

        $road_name = "ScaleUp/".$username;

        $this->filesystem->createDir($road_name.'/'.'marketing');
        $this->filesystem->createDir($road_name.'/'.'law');
        $this->filesystem->createDir($road_name.'/'.'finance');
        $this->filesystem->createDir($road_name.'/'.'strategy');
      
        
        foreach ($docs as $key => $doc) {
          
            $road = $this->createFileInOneDrive($doc,$road,$road_name,$key);
        }   
    
    }


    public function getUserOneDrive($username)
    {
        $users_path = "ScaleUp";
        $this->filesystem->deleteDir($users_path.'/'.$username);
        
        $listing = $this->filesystem->createDir($users_path.'/'.$username);

    }

    public function createFileInOneDrive($doc,$road,$road_name,$k)
    {
       
        $path = $doc->getRelativePath();

        $pathes = explode("/",$path);
        
        foreach ($pathes as $key => $path) {
        
            $road_name_old = $road_name;

            $road_name_new = $road_name."/".$path;
            
            $next_folder = null;
            if(isset($road[$road_name_new])){
                $next_folder = $road[$road_name_new];
                $road_name = $road_name_new;
            }

            if(!$next_folder){
                $this->filesystem->createDir($road[$road_name_old].'/'.$path);

                $road[$road_name_new] = $road[$road_name_old].'/'.$path;

                $road_name = $road[$road_name_old].'/'.$path;
            }
        }

        $contents = File::get($doc->getPathName());
        $this->filesystem->put($road[$road_name].'/'.$doc->getFileName(),$contents);

        return $road;
    }
}
