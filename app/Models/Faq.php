<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'text'
    ];

    public function createModel($request,$faqCategory)
    {
        return $faqCategory->faqs()->create($request);
    }

    public function updateModel($request)
    {
        foreach ($request['faqs'] as $key => $faq) {
           
            $this->where('id',$faq['id'])->update(['title'=>$faq['title'],'text'=>$faq['text']]);
        }
    }

    public function destroyModel($faq)
    {
        $faq->delete();

        return response()->json([
            "status"=>"success"
        ], 204);
    }
}
