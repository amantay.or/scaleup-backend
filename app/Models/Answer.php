<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Log;
use File;

class Answer extends Model
{
    use HasFactory;

    protected $fillable = ['user_id','form_id','answers','disabled','commentary_status','section_status'];
    
    protected $casts = [
      //  'answers' => 'array',
    ];

    public function getAnswersAttribute($value) {
    
        return json_decode($value);
    }

    public function commentary()
    {
        return $this->hasOne('App\Models\Commentary');
    }


    public function storeModel($request)
    {
      
       
            $section = $request['section'];
            if($request['answers'] == "empty")
                $request['answers'] = [];

            if($request['repeated'] == "empty")
                $request['repeated'] = [];

            $this->deleteFiles($request,auth()->user());
            
          
            $answers = $request['answers'];

         
            $key_repeated = 0;
            foreach ($answers as $key => $answer) {
             
               $img = $this->storeFile($answer,auth()->user());
               
               $request['answers'][$key] = $img;

               $key_repeated = $key+1;
            }

            $repeated = $request['repeated'];

            foreach ($repeated as $key => $answer) {
                
                $request['answers'][$key_repeated] = $answer;

                $key_repeated++;
            }

        if($request['answers'] == [] || $request['answers'] == "empty"){
            auth()->user()->answers()->where('form_id',$request['form_id'])->update(['answers'=>null,'section_status'=>$section]);
            return response()->noContent();
        }
        else{
            return auth()->user()->answers()->updateOrCreate(
                ['form_id'=>$request['form_id']],
                ['answers' =>json_encode($request['answers']),'section_status'=>$section]
            );
        }
        
    }

    public function storeManyModel($request,$user = null)
    {
      
        $section = $request['section'];
       
        if(!$user)
            $user = auth()->user();
        else{
            if(!$user->production){
                return response()->json(['status'=>'error','message'=>"Анкета еще не сохранена!"],403);
            }
        }
        
        foreach ($request['data'] as $k => $value) {

            if($value['answers']){
                $value['answers'] = json_encode($value['answers']);
            }
                
       //  if($value['answers'] == "null" || )
            $user->answers()->updateOrCreate(
                ['form_id'=>$value['form_id']],
                ['answers' => $value['answers'],'section_status'=>$section]
            );
           
        }

        $user->answers()->where('commentary_status',2)->update(['disabled'=>1,'commentary_status'=>0]);

        
    }

    public function index()
    {
        $answers = auth()->user()->answers->keyBy('form_id');
      
        return $answers;
    }


    public function storeByWorker($request,$user)
    {
      
       
            $section = $request['section'];
            if($request['answers'] == "empty")
                $request['answers'] = [];

            if($request['repeated'] == "empty")
                $request['repeated'] = [];

            $this->deleteFiles($request,$user);
            
          
            $answers = $request['answers'];

         
            $key_repeated = 0;
            foreach ($answers as $key => $answer) {
             
               $img = $this->storeFile($answer,$user);
               
               $request['answers'][$key] = $img;

               $key_repeated = $key+1;
            }

            $repeated = $request['repeated'];

            foreach ($repeated as $key => $answer) {
                
                $request['answers'][$key_repeated] = $answer;

                $key_repeated++;
            }

        if($request['answers'] == [] || $request['answers'] == "empty"){
            $user->answers()->where('form_id',$request['form_id'])->update(['answers'=>null,'section_status'=>$section]);
            return response()->noContent();
        }
        else{
            return $user->answers()->updateOrCreate(
                ['form_id'=>$request['form_id']],
                ['answers' =>json_encode($request['answers']),'section_status'=>$section]
            );
        }
        
    }

    private function storeFile($file,$user)
    {
        
        $fileName = time().'-'.uniqid().'.'.$file->extension();

        $path = $file->storeAs('answers/'.$user->id.'/',$fileName);
       
      //  Storage::put('invoices/'.$fileName, $file);
            
        return 'answers/'.$user->id.'/'.$fileName;
    }

    private function deleteFiles($request,$user){
        $answers = $user->answers()->where('form_id',$request['form_id'])->first();
        if(!$answers)
            return;
        $answers = $answers['answers'];

        if(!$answers)
            return;
        foreach ($answers as $key => $answer) {
            if(File::exists(storage_path('app/'.$answer)) && !in_array($answer, $request['repeated']) ){
                unlink(storage_path('app/'.$answer));
            }
        }
       
    }

}
