<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Production extends Model
{
    use HasFactory,SoftDeletes;

    protected $fillable = [
        'r_strategy',
        'r_legal',
        'r_marketing',
        'r_financial',
        'strategy',
        'financial',
        'legal',
        'marketing',
        'status',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
