<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Production;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\FileModel;
use App\Models\Notification;

class Worksheet extends Model
{
    use HasFactory,SoftDeletes;
    

    const NOTREADY= 0;
    const READY   = 1;
    const PENDING = 2;
    const SAVED   = 3;
    const FINISH  = 4;

    protected $fillable = ['user_id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function pending($request)
    {
        if(!isset(auth()->user()->worksheet)){
            return response()->json(['status'=>'success','message'=>'Сохранено']);
        }
        if(auth()->user()->worksheet[$request['type']] == Worksheet::READY){
            auth()->user()->worksheet()->update([$request['type'] => Worksheet::PENDING]);
            return response()->json(['status'=>'success']);
        }
            
        return response()->json(['status'=>'success','message'=>'Статус анкеты уже Pending или больше']);
    }

    public function savedState($request)
    {
        if(!isset(auth()->user()->worksheet)){
            return response()->json(['status'=>'success','message'=>'Сохранено']);
        }
        if(auth()->user()->worksheet[$request['type']] != Worksheet::SAVED){
            auth()->user()->worksheet()->update([$request['type'] => Worksheet::SAVED]);
            return response()->json(['status'=>'success']);
        }

        return response()->json(['status'=>'success','message'=>'Статус анкеты уже Saved']);
    }

    public function disableAnswers(){

        $user = auth()->user();
       
        $user->answers()->update(['disabled'=>1]);

    }

    public function submit()
    {
        $worksheet = auth()->user()->worksheet;
     
        if( $worksheet->strategy == Worksheet::SAVED && $worksheet->financial == Worksheet::SAVED &&
            $worksheet->legal == Worksheet::SAVED && $worksheet->marketing == Worksheet::SAVED && 
            $worksheet->sended == Worksheet::NOTREADY
            ) {

                $worksheet->sended = Worksheet::SAVED;
                $worksheet->save();

                $fileModel = new FileModel();

                $fileModel->createIndustry($worksheet->user_id);

                $worksheet->delete();

                $this->disableAnswers();

                Production::create(['user_id'=>$worksheet->user_id]);

                $client = auth()->user();
                $company = $client->company;
                $users = User::whereHas("roles", function($q){ $q->where("name", "moderator")->orWhere('name','admin'); })->get();
                foreach ($users as $key => $user) {
                    Notification::notify("$company отправил анкету на проверку",$user);
                }

                return response()->json(['status'=>'success','message'=>'OK']); 
            }

            return response()->json(['status'=>'error','message'=>'Не все анкеты сохранены или действие уже сделано!'],403);   
        
    }


    public function ready($worksheet)
    {
        $role = auth()->user()->roles[0]->name;

        if($role != "admin" && $role != "moderator"){
            return response()->json(['status'=>'error','message'=>'ВЫ не админ и не модератор!'],403);
        }

        if( $worksheet->strategy == Worksheet::SAVED && $worksheet->financial == Worksheet::SAVED &&
            $worksheet->legal == Worksheet::SAVED && $worksheet->marketing == Worksheet::SAVED && 
            $worksheet->sended == Worksheet::SAVED
            ) {

                $worksheet->sended = Worksheet::FINISH;
                $worksheet->save();
                
                $worksheet->delete();

                
                
                return response()->json(['status'=>'success','message'=>'OK']); 
            }
            
            return response()->json(['status'=>'error','message'=>'Ракета еще не взлетела!'],403); 
    }   
}
