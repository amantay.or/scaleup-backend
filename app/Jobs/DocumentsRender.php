<?php

namespace App\Jobs;

use App\Models\Production;
use App\Models\Document;
use App\Models\FileModel;
use App\Models\User;
use App\Models\Notification;
use DB;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Log;

class DocumentsRender implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $production;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Production $production)
    {
        $this->production = $production;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $production = $this->production;

        $fileModel = new FileModel();
        
        $fileModel->createUserFiles($production->user_id);

        DB::table('documents')->updateOrInsert(
            ['user_id'=>$production->user_id],
            ['strategy'=>0,'financial'=>0,'legal'=>0,'marketing'=>0,'status'=>2]
        );
        $document = Document::where('user_id',$production->user_id)->first();

        $document->storeGoogleDisk();
        
        $company = $production->user->company;

        $users = User::whereHas("roles", function($q){ 
            $q->where("name", "editor")
                ->orWhere("name", "financier")
                ->orWhere("name", "lawyer") 
                ->orWhere("name", "marketer")
                ->orWhere('name','admin');  
        })->get();
        foreach ($users as $key => $user) {
            Notification::notify("$company готово к проверке",$user);
        }
        $production->status = 1;
        $production->save();

        $document->status = 0;
        $document->save();

    }
}
