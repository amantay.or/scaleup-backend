<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Microsoft\Graph\Graph;
use League\Flysystem\Filesystem;
use NicolasBeauvais\FlysystemOneDrive\OneDriveAdapter;
use DB;
use App\Models\Notification;
use App\Models\User;
use App\Models\Document;
use App\Models\PublicDocument;
use Illuminate\Support\Str;
use File;

class ZipRender implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $document;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Document $document)
    {
        $this->document = $document;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $graph = new Graph();

        $onedrive = DB::table('onedrive')->first();

        $graph->setAccessToken($onedrive->access_token);

        $adapter = new OneDriveAdapter($graph, 'root');

        $filesystem = new Filesystem($adapter);
        $user_id = $this->document->user_id;
        $files = $filesystem->listContents("ScaleUp/$user_id",true);
        
        $zip_file = storage_path("app/documents/users/$user_id/archive.zip");

        $zip = new \ZipArchive();
        $zip->open($zip_file, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
        
        foreach ($files as $key => $file) {
            if($file['type'] == 'file'){
                $content = $filesystem->read($file['path']);
                $zip->addFromString($file['path'], $content);
            }            
        }
        $zip->close();

        $contents = File::get($zip_file);
        $filesystem->put("ScaleUp/$user_id/archive.zip",$contents);

        $this->document->status = 1;
        $this->document->save();
        
        User::find($this->document->user_id)->update(['docs'=>1]);
        
        self::publicDocumentRender($this->document);

        Notification::notify("Ваши документы готовы",$this->document->user);

    }

    public static function publicDocumentRender($document){
      
        $arr = [];
        foreach ([0,1] as $value1) {
            foreach ([0,1] as $value2) {
                foreach ([0,1] as $value3) {
                    foreach ([0,1] as $value4) {
                        $arr[] = ['user_id'=>$document->user_id,'token'=>time().Str::random(40), 'strategy'=>$value1,'financial'=>$value2,'legal'=>$value3,'marketing'=>$value4];
                    }
                }
            }
        }
        PublicDocument::insert($arr);
    }
}
