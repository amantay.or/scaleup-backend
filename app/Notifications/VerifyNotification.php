<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Log;
use Illuminate\Auth\Notifications\VerifyEmail;

class VerifyNotification extends VerifyEmail
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $link = env("FRONT_URL")."verify/";


        $verificationUrl = $this->verificationUrl($notifiable);
        
        $params = explode("verify/",$verificationUrl)[1];

        $link .= $params;

        return (new MailMessage)
            ->subject('Добро пожаловать в Scaleup!')
            ->line('Здравствуйте,')
            ->line('Спасибо за регистрацию в Scaleup.')
            ->line('Чтобы иметь возможность зарегистрироваться в нашу систему просим вас подтвердить электронный адрес:')
            ->action('Подтвердить аккаунт', $link);
           
           

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
