<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Log;

class WorkerCreate extends Notification implements ShouldQueue
{
    use Queueable;

    protected $user;
    protected $password;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user,$password)
    {
        $this->user = $user;
        $this->password = $password;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Добро пожаловать в Scaleup!')
                    ->line('Здравствуйте!')
                    ->line('В система для оформления франшизы Scaleup Вам создан акаунт.')
                    ->line('Роль: '.$this->user->roles[0]->name)
                    ->line('Логин: '.$this->user->email)
                    ->line('Пароль: '.$this->password)
                    ->line('Ссылка для авторизации: '.env("FRONT_URL"));
                    
                   
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
