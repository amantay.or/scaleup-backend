<?php

namespace App\Http\Resources\Question;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Form\FormResource;
class QuestionResource extends JsonResource
{
    
   
    
    public function toArray($request)
    {
       //dd($this->foo);
      
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'example' => $this->example,
         
            'forms' => FormResource::collection($this->forms)
            
        ];
    }

}
