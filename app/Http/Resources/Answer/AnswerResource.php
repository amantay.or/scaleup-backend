<?php

namespace App\Http\Resources\Answer;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Commentary\CommentaryResource;

class AnswerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'form_id' => $this->form_id,
            'answers' => $this->answers,
            'disabled' => $this->disabled,
            'commentary' => new CommentaryResource($this->commentary),
            
        ];
    }
}
