<?php

namespace App\Http\Resources\Payment;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\User\UserResource;

class PaymentResourceWithInvoice extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $service = $this->service;
        $paymentStatus = $this->paymentStatus;
        $paymentType = $this->paymentType;
        $user = new UserResource($this->user);
        $invoice = $this->invoice;
        
        
        return [
            'id' => $this->id,
            'payment_status' => ['id'=>$paymentStatus->id,'name'=>$paymentStatus->name],
            'service' => ['id'=>$service->id,'name'=>$service->name,'price'=>$service->price],
            'payment_type' =>['id'=>$paymentType->id,'name'=>$paymentType->name],
            'user'=>$user,
            'invoice'=>$invoice,
        ];
    }
}
