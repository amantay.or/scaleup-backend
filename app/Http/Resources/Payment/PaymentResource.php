<?php

namespace App\Http\Resources\Payment;

use Illuminate\Http\Resources\Json\JsonResource;

class PaymentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        
        
        $service = $this->service;
        
        $paymentStatus = $this->paymentStatus;
        $paymentType = $this->paymentType;
        
        return [
            'id' => $this->id,
            'payment_status' => ['id'=>$paymentStatus->id,'name'=>$paymentStatus->name],
            'service' => ['id'=>$service->id,'name'=>$service->name,'price'=>$service->price],
            'payment_type' =>['id'=>$paymentType->id,'name'=>$paymentType->name],
            'created_at'=>$this->updated_at
        ];
    }
}
