<?php

namespace App\Http\Resources\Worksheet;

use Illuminate\Http\Resources\Json\JsonResource;

class WorksheetResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'strategy' => $this->strategy,
            'financial' => $this->financial,
            'legal' => $this->legal,
            'marketing' => $this->marketing,
            'sended' => $this->sended,
            'user'=>$this->user
        ];

    }
}
