<?php

namespace App\Http\Resources\Form;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Answer\AnswerResource;

class FormResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if(auth()->user()->roles[0]->name == "admin"){
           
        //    if($this->id == 127){
        //        dd($this->answers);
        //    }
            return [
                'id' => $this->id,
                'question_id' => $this->question_id,
                'title' => $this->title,
                'placeholder' => $this->placeholder,
                'type' => $this->type,
                'input' => $this->input,
                'answers' => $this->answers,
                'key' => $this->key,
                'answer' => new AnswerResource($this->answer)
            ];
        }

        return [
            'id' => $this->id,
            'question_id' => $this->question_id,
            'title' => $this->title,
            'placeholder' => $this->placeholder,
            'type' => $this->type,
            'input' => $this->input,
            'answers' => $this->answers,
            'key' => $this->key,
            
            'answer' => new AnswerResource($this->answer),
            

           // 'forms' => $this->forms()->with(['answer','answer.commentary'])->get()
            
        ];
    }
}
