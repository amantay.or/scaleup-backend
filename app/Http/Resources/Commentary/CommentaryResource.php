<?php

namespace App\Http\Resources\Commentary;

use Illuminate\Http\Resources\Json\JsonResource;

class CommentaryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'answer_id' => $this->answer_id,
            'text' => $this->text,
            'read' => $this->read,
            'created_at'=>$this->created_at,
            'user' => $this->user->load('roles')  
        ];
    }
}
