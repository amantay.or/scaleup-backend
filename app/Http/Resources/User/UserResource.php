<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $payment = $this->payment;
        
        $role = $this->roles;
        
        $industry = $this->industry;

        $production = $this->production;

        $document = $this->document;

        $worksheet = $this->worksheet;

        $public_document = $this->public_document;

        return parent::toArray($request);
    }
}
