<?php

namespace App\Http\Requests\BLock;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBlockRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'greeting' =>'required',
            'image' =>'file',
            'description' =>'required',
            'link' =>'required',
            'changed'=>'required|boolean',
        ];
    }
}
