<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fio'=>'required|min:4',
            'company'=>'required',
            'phone'=>'required|integer',
            'email'=>'required|unique:users',
            'password'=>'required|confirmed',
        ];
    }

    public function messages()
    {
        return [
            'fio.required' => 'Заполните ФИО',
            'company.required' => 'Заполните название компании',
            'phone.required' => 'Заполните номер телефона',
            'email.required' => 'Заполните почту',
            'password.required' => 'Заполните пароль',
            'fio.min' => 'Количество символов в ФИО должно превышать 4',
            'phone.integer' => 'Номер телефона принимает только числа',
            'email.unique' => 'Эта почта уже существует',
            'password.confirmed' => 'Пароли не совпадают',
        ];
    }

    

    
}
