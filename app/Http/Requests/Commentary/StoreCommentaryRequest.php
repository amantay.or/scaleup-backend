<?php

namespace App\Http\Requests\Commentary;

use Illuminate\Foundation\Http\FormRequest;

class StoreCommentaryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'answer_id'=>'required',
            'text'=>'required',
            'section'=>'required',
            'user_id'=>'required'
        ];
    }
}
