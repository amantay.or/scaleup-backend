<?php

namespace App\Http\Requests\Worker;

use Illuminate\Foundation\Http\FormRequest;

class StoreWorkerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fio'=>'required',
            'role_id'=>'required|integer',
            'email'=>'required|unique:users',
            'password'=>'required|confirmed',
            'phone' =>'required|integer'
        ];
    }

    public function messages()
    {
        return [
            'fio.required' => 'Заполните ФИО',
            'email.required' => 'Заполните почту',
            'password.required' => 'Заполните пароль',
            'email.unique' => 'Эта почта уже существует',
            'password.confirmed' => 'Пароли не совпадают',
            'phone.required' => 'Заполните номер телефона',
        ];
    }
}
