<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use Illuminate\Http\Request;
use App\Http\Requests\Payment\StorePaymentRequest;
use App\Http\Requests\Payment\SelectPaymentRequest;
use Illuminate\Support\Facades\Response;
use App\Http\Requests\Payment\StoreInvoiceRequest;
use App\Http\Requests\Payment\AcceptedRequest;
use Illuminate\Support\Facades\Storage;
use App\Models\User;
use App\Http\Resources\Payment\PaymentResourceWithInvoice;
use Illuminate\Support\Facades\Gate;

class PaymentController extends Controller
{

    protected $payment;
    
    public function __construct(Payment $payment)
    {
        $this->payment = $payment;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePaymentRequest $request)
    {
        $request = $request->validated();

        $request['user_id'] = auth()->user()->id;
        return $this->payment->storeModel($request);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Payment $payment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Payment $payment)
    {
        //
    }

    public function getPayment()
    {
        return $this->payment->getPayment();
    }

    public function select(SelectPaymentRequest $request)
    {
        if (! Gate::allows('select-payment')) {
            abort(403);
        }
        $request = $request->validated();
        return $this->payment->selectPayment($request);
    }

    public function getInvoiceFile()
    {
        $path = public_path().'/files/company-files/'.'company-invoice.pdf';
        return Response::download($path);
    }

    public function invoice(StoreInvoiceRequest $request)
    {
        
        $request = $request->validated();
    

        return $this->payment->storeInvoice($request);
    }

    public function bank(Request $request)
    {
        return $this->payment->storeBank();
    }

    public function downloadInvoice()
    {
        return Storage::download('invoices/UmCo3euAQ7pLFlLTOIa6lI9asggOMyfHVDDIBOTj.pdf');
    }

    public function accepted(User $user)
    {
        return $this->payment->accepted($user);
    }

    public function list()
    {
        $payments = $this->payment->list();
        return PaymentResourceWithInvoice::collection($payments);
    }

    public function show(User $user)
    {
        $payment = $this->payment->show($user);
        return new PaymentResourceWithInvoice($payment);
    }

}
