<?php

namespace App\Http\Controllers;

use App\Models\Production;
use App\Models\Document;
use Illuminate\Http\Request;
use App\Http\Requests\Production\UpdateProductionRequest;
use App\Models\FileModel;
use App\Models\User;
use App\Models\Notification;
use DB;
use Log;
use App\Jobs\DocumentsRender;

class ProductionController extends Controller
{
    protected $production;

    public function __construct(Production $production)
    {
        $this->production = $production;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      
        if(isset($request->status)){
            if($request->status == 0)
                $this->production = $this->production->where([['strategy',0],['financial',0],['legal',0],['marketing',0]]);
            else if($request->status == 1)
                $this->production = $this->production->where('strategy',1)->orWhere('financial',1)->orWhere('legal',1)->orWhere('marketing',1)
                    ->orWhere('strategy',2)->orWhere('strategy',2)->orWhere('legal',2)->orWhere('marketing',2);
            else if($request->status == 3)
                $this->production = $this->production->where([['strategy',3],['financial',3],['legal',3],['marketing',3]]);
        }
        if(isset($request->commenary)){
            if($request->commenary == 0)
                $this->production = $this->production->where([['strategy','!=',0],['financial','!=',0],['legal','!=',0],['marketing','!=',0]]);
            if($request->commenary == 1)
                $this->production = $this->production->where('strategy',2)->orWhere('financial',2)->orWhere('legal',2)->orWhere('marketing',2);
        }
        if(isset($request->industries)){

            $industries = explode(",", $request->industries);
            
            $this->production = $this->production->whereHas('user',function($q) use($industries){
                $q->whereIn('industry_id',$industries);
            });
        }
        if(isset($request->from)){
            $from =$request->from;
           
            $this->production = $this->production->whereHas('user',function($q) use($from){
                $q->where('email_verified_at','>=',$from);
            });
        }
        if(isset($request->to)){
            $to = $request->to;
            $this->production = $this->production->whereHas('user',function($q) use($to){
                $q->where('email_verified_at','<=',$to);
            });
        }

        
        $this->production = $this->production->with('user')->get();

        return $this->production;
    }

    
    public function update(UpdateProductionRequest $request, Production $production)
    {
        $request = $request->validated();

        $role = auth()->user()->roles[0]->name;

        if($role == "moderator" || $role == "admin"){
            return $this->updateEditor($request,$production);
        }
        else{
            return response()->json(['status'=>'success',"message"=>'У вас не хватает разрешения для этого действия'],403); 
        }

    }

    public static function updateEditor($request, $production){

        // 0 - К проверке
        // 1 - Проверяю 
        // 2 - Требуется исправление
        // 3 - Готов

        switch ($request['section']) {
            case 'strategy':
                $production->update(['strategy'=>$request['type']]);
                break;
            case 'legal':
                $production->update(['legal'=>$request['type']]);
                break;
            case 'financial':
                $production->update(['financial'=>$request['type']]);
                break; 
            case 'marketing':
                $production->update(['marketing'=>$request['type']]);
                break; 
            
        }
        return response()->json(['status'=>'success']); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Production  $production
     * @return \Illuminate\Http\Response
     */
    public function ready(Production $production)
    {
        $role = auth()->user()->roles[0]->name;
        
        if($role == "admin" || $role == "moderator"){

            if( $production->strategy == 3 && $production->financial == 3 &&
                $production->legal == 3 && $production->marketing == 3 && $production->status == 0){ // && $production->status == 0
                   
                    $docJob = new DocumentsRender($production);
                    dispatch($docJob);

                    return response()->json(['status'=>'success']); 
                }

                return response()->json(['status'=>'success',"message"=>'Сперва все анкеты должны быть готовы'],403); 
        }
        return response()->json(['status'=>'success',"message"=>'У вас не хватает разрешения для этого действия'],403);  
    }
}
