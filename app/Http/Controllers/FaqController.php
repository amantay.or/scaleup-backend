<?php

namespace App\Http\Controllers;

use App\Models\Faq;
use App\Models\FaqCategory;
use Illuminate\Http\Request;
use App\Http\Requests\Faq\StoreFaqRequest;
use App\Http\Requests\Faq\UpdateFaqRequest;

class FaqController extends Controller
{
    protected $faq;

    protected $faqCategory;
    
    public function __construct(Faq $faq,FaqCategory $faqCategory)
    {
        $this->faq = $faq;
        $this->faqCategory = $faqCategory;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->faqCategory->with('faqs')->get();
        return $this->faq->get();
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreFaqRequest $request,FaqCategory $faqCategory)
    {
        $request = $request->validated();

        return $this->faq->createModel($request,$faqCategory);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateFaqRequest $request)
    {
        $request = $request->validated();

        return $this->faq->updateModel($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function destroy(Faq $faq)
    {
        return $this->faq->destroyModel($faq);
    }
}
