<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Industry;
use App\Http\Requests\Industry\StoreIndustryRequest;

class IndustryController extends Controller
{
    protected $industry;
    
    public function __construct(Industry $industry)
    {
        $this->industry = $industry;
    }

    public function index()
    {
        return $this->industry->get();
    }

    public function store(StoreIndustryRequest $request)
    {
        $request = $request->validated();

        $industry = $this->industry->create($request);
        
        return $industry;
    }

    public function destroy(Industry $industry)
    {
        $industry->delete();
        
        return response()->noContent();
    }
}
