<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use App\Http\Requests\Permission\StorePermissionRequest;

class PermissionController extends Controller
{
    protected $permissions;
    
    public function __construct(Permission $permissions)
    {
       
        $this->permissions = $permissions;

    }

    public function index()
    {
        return $this->permissions->get();
    }

    public function store(StorePermissionRequest $request)
    {
        $request = $request->validated();

        $permission = $this->permissions->create($request);
        
        return $permission;
    }

    public function destroy(Permission $permission)
    {
        $permission->delete();
        
        return response()->noContent();
    }
}
