<?php

namespace App\Http\Controllers;

use App\Models\Block;
use Illuminate\Http\Request;
use App\Http\Requests\BLock\UpdateBlockRequest;

class BlockController extends Controller
{

    protected $block;
    
    public function __construct(Block $block)
    {
        $this->block = $block;
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Block  $block
     * @return \Illuminate\Http\Response
     */
    public function showFirst()
    {
        return $this->block->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Block  $block
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBlockRequest $request)
    {
       
        $request = $request->validated();
        return $this->block->updateModel($request);
       
    }


}
