<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\User\StoreUserRequest;
use App\Http\Requests\User\LoginUserRequest;
use App\Http\Requests\User\CheckLoginRequest;
use App\Http\Requests\User\StoreUserAvatar;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Password;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Notifications\ResetPassword;
use App\Http\Resources\User\UserResource;
use Log;
use App\Models\Payment;
use File;
use App\Models\Notification;

class AuthController extends Controller
{
    protected $users;

    protected $roles;

    public function __construct(User $users, Role $roles)
    {
        $this->users = $users;
        
        $this->roles = $roles;
    }
    
    public function register(StoreUserRequest $request)
    {
        $validatedData = $request->validated();

        $validatedData['password'] = bcrypt($request->password);

        $user = $this->users->create($validatedData);
        
        $verifyUser = $user;
       
        $company = $user->company;
        $users = User::whereHas("roles", function($q){ $q->where("name", "sale_department")->orWhere('name','admin'); })->get();
        
        foreach ($users as $key => $user_role) {
            Notification::notify("Новый зарегистрированный клиент $company",$user_role);
        }
        
        $user->assignRole($this->roles->find(1));

        Payment::create(['user_id'=>$user->id,'payment_status_id'=>1,'payment_type_id'=>3,'service_id'=>4]);

        try {
            $verifyUser->sendEmailVerificationNotification();
        } catch (\Throwable $th) {
            Log::info($th);
            return response([ 'status' => 'success','message'=>'email не отправилось']);
        }


        return response([ 'status' => 'success']);
    }

    public function login(LoginUserRequest $request)
    {
        $loginData = $request->validated();


        if (!auth()->attempt($loginData)) {
            return response(['message' => 'Invalid Credentials'],401);
        }
        if(!auth()->user()->hasVerifiedEmail()) {
            return response(['message' => "Your email address is not verified."],401);
        }

        $accessToken = auth()->user()->createToken('authToken')->accessToken;

        return response(['user'=>new UserResource(auth()->user()),'access_token' => $accessToken]);
    }

    public function forgot() {

        $credentials = request()->validate(['email' => 'required|email|exists:users']);

        
        $a = Password::sendResetLink($credentials);
       
        return response()->json(["msg" => 'Reset password link sent on your email.']);
    }
    
    public function reset() {
        $credentials = request()->validate([
            'email' => 'required|email',
            'token' => 'required|string',
            'password' => 'required|string|confirmed'
        ]);

        $reset_password_status = Password::reset($credentials, function ($user, $password) {
            $user->password = bcrypt($password);
            $user->save();
        });

        if ($reset_password_status == Password::INVALID_TOKEN) {
            return response()->json(["msg" => "Invalid token provided"], 400);
        }

        return response()->json(["msg" => "Password has been successfully changed"]);
    }

    public function reset_password(Request $request){
        $credentials = $request->validate([
            'email' => 'required|email',
            'token' => 'required|string',
        ]);
       
        return response()->json($credentials);
    }

    public function verify($user_id, Request $request) {
        if (!$request->hasValidSignature()) {
            return response()->json(["msg" => "Invalid/Expired url provided."], 401);
        }
    
        $user = User::findOrFail($user_id);

        if (!$user->hasVerifiedEmail()) {
            $user->markEmailAsVerified();
        }

        return response()->json(["status" => "succcess"], 200);

        
    }

    public function resend() {
        if (auth()->user()->hasVerifiedEmail()) {
            return response()->json(["msg" => "Email already verified."], 400);
        }
    
        auth()->user()->sendEmailVerificationNotification();
    
        return response()->json(["msg" => "Email verification link sent on your email id"]);
    }

    public function checkLogin(CheckLoginRequest $request)
    {
        $request = $request->validated();
        try
        {
            $this->users->where('email',$request['email'])->firstOrFail();
            return response()->noContent();
        }
        catch(ModelNotFoundException $e)
        {
           return response()->json(['message'=>'Email не найден!'],404);
        }
        
    }

    public function showMe()
    {
        if(isset(auth()->user()->onlyPayment)) {
            auth()->user()->onlyPayment->checkBank();
            unset(auth()->user()->onlyPayment);
        }
        
        return new UserResource(auth()->user());
    }

    public function showUser(User $user)
    {
        return new UserResource($user);
    }

    public function saveAvatar(StoreUserAvatar $request)
    {
        $request = $request->validated();

        $user = auth()->user();

        $unix_time = time();
       
      
        $image_parts = explode(";base64,", $request['avatar']);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);

     
        $file =  "images/users/".uniqid() . '.'.$image_type;
      
        file_put_contents($file, $image_base64);

        if (!is_null($user->avatar)) {
            File::delete($user->avatar);
        }
        $user->avatar = $file;
        $user->save();

        return new UserResource($user);
    }
    
}
