<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Http\Requests\Role\StoreRoleRequest;
use App\Http\Requests\Role\StorePermissionToRoleRequest;

class RoleController extends Controller
{

    protected $roles;
    
    public function __construct(Role $roles,Permission $permissions)
    {
        $this->roles = $roles;
        $this->permissions = $permissions;
    }

    public function index()
    {
        return $this->roles->get();
    }

    public function store(StoreRoleRequest $request)
    {
        $request = $request->validated();

        $role = $this->roles->create($request);
        
        return $role;
    }

    public function destroy(Role $role)
    {
        $role->delete();

        return response()->noContent();
    }

    public function permissionToRole(StorePermissionToRoleRequest $request)
    {
        $request = $request->validated();
       
        $role = $this->roles->find($request['role_id']);
       
        $permission = $this->permissions->find($request['permission_id']);

        $role->givePermissionTo($permission);

        return $role;
        
    }


}
