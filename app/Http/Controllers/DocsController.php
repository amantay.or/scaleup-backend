<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use App\Models\User;
use File;
use Log;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use App\Http\Requests\Payment\InvoiceCreateRequest;
use PhpOffice\PhpSpreadsheet\Excel;
use DB;

class DocsController extends Controller
{
    public function index(){

        $storage = Storage::disk('google')->allFiles();
        dd($storage);
        
    }
    public function store(){

        $this->createFromLocal();

    }

    public function createFromLocal()
    {
        

        $username = "8-Amantay Amantay";

        $docs = File::allFiles(storage_path('app/documents/users/'.$username));
        
        $user = $this->getUser($username);

        $road = ["8-Amantay Amantay"=>$user['path']];

        $road_name = $user['name'];
        
        
        foreach ($docs as $key => $doc) {
           // $s = microtime(true);
            $road = $this->createFileInDrive($doc,$road,$road_name,$key);
         //   Log::info(microtime(true) - $s);
        }   
    
    }

    public function createFileInDrive($doc,$road,$road_name,$k)
    {
       
        $path = $doc->getRelativePath();

        $pathes = explode("/",$path);
        
        foreach ($pathes as $key => $path) {
        
            $road_name_old = $road_name;

            $road_name_new = $road_name."/".$path;
            
            $next_folder = null;
            if(isset($road[$road_name_new])){
                $next_folder = $road[$road_name_new];
                $road_name = $road_name_new;
            }

            if(!$next_folder){
                
                Storage::disk('google')->makeDirectory($road[$road_name_old].'/'.$path);
                
                $s = microtime(true);
                
                $data = collect(Storage::disk('google')->listContents($road[$road_name_old], false));

                $next_folder = $data->where('filename', '=', $path)->first();

                $road[$road_name_new] = $next_folder['path'];

                $road_name = $road_name_new;
            }
        }
       

        Storage::disk('google')->put($road[$road_name].'/'.$doc->getFileName(), File::get($doc->getPathName()));
        return $road;
    }

    public function getUser($username)
    {
        $users_path = "/1SfQBBNzmcUrPPssDe4TBpM1c6RJUbORw";

        $users = collect(Storage::disk('google')->listContents($users_path, false));
        $user = $users->where('filename', '=', $username)->first();

        if($user)
            Storage::disk('google')->deleteDirectory($users_path.'/'.$user['path']);

        Storage::disk('google')->makeDirectory($users_path.'/'.$username);

        $users = collect(Storage::disk('google')->listContents($users_path, false));
        $user = $users->where('filename', '=', $username)->first();

        return $user;
    }
    
    public function invoiceCreate(InvoiceCreateRequest $request)
    {
        $request = $request->validated();
        $user = auth()->user();

        $payment = $user->payment;

        $service = $payment->service;

        $id = str_pad($payment->id, 6, '0', STR_PAD_LEFT);
        $date = $payment->updated_at->format('d.m.Y');
        $info = $request['info'];
        $fio = $request['fio'];

        $invoice = DB::table('files')->where('id',1)->first()->name;
        
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($invoice);

       $foundInCells = [
        '${info}'=>$info,
        '${id}'=>$id,
        '${datetime}'=>$date,
        '${company}'=>$user->company,
        '${service}'=>$service->name,
        '${price}'=>$service->price,
        '${fio}'=>$fio,
       ];
       foreach ($foundInCells as $key => $value) {
            foreach ($spreadsheet->getWorksheetIterator() as $worksheet) {
                $ws = $worksheet->getTitle();
                foreach ($worksheet->getRowIterator() as $row) {
                    $cellIterator = $row->getCellIterator();
                    $cellIterator->setIterateOnlyExistingCells(true);
                    foreach ($cellIterator as $cell) {
                        if (strpos($cell->getValue(),$key) !== false) {
                            $cell_value = str_replace($key,$value,$cell->getValue());
                            $worksheet->setCellValue($cell->getCoordinate(), $cell_value);
                        }
                    }
                }
            }
        }
        $writer = new Xlsx($spreadsheet);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="invoice.xlsx"');

        ob_start();
        $writer->save('php://output');
        $content = ob_get_contents();
        ob_end_clean();

        File::put("invoices/Счет на оплату № $id от $date.xlsx", $content,0755); 
        
        return response()->json([
            'link' => "http://platformapi.scaleup.plus/invoices/Счет на оплату № $id от $date.xlsx"
        ]);

    }
}
