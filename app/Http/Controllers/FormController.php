<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Section;
use App\Models\Question;
use App\Models\Form;
use App\Http\Resources\Section\SectionResource;

class FormController extends Controller
{
    public function update(Request $request){

        if($request->type == "section"){

            Section::find($request['id'])->update([
                'name'=>$request->name,
                'description'=>$request->description,
                'link'=>$request->link
            ]);

        }
        else if($request->type == "question"){

            Question::find($request['id'])->update([
                'title'=>$request->title,
                'description'=>$request->description,
                'example'=>$request->example
            ]);

        }
        else if($request->type == "form"){

            Form::find($request['id'])->update([
                'title'=>$request->title,
                'placeholder'=>$request->placeholder,
                'answers'=>json_encode($request->answers),
                'key'=>$request->key
            ]);

        }

    }

    public function index()
    {
        $sections = Section::get();

        return $sections;
    }

    public function indexSection(Section $section)
    {
       
        return new SectionResource($section);
    }
}
