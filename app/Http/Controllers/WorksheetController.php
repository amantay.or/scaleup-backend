<?php

namespace App\Http\Controllers;

use App\Models\Worksheet;
use Illuminate\Http\Request;
use App\Http\Resources\Worksheet\WorksheetResource;
use App\Http\Requests\Worksheet\PendingWorksheetRequest;

class WorksheetController extends Controller
{

    protected $worksheet;

    public function __construct(Worksheet $worksheet)
    {
        $this->worksheet = $worksheet;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(isset($request->status)){
            if($request->status == 1)
                $this->worksheet = $this->worksheet->where([['strategy',1],['financial',1],['legal',1],['marketing',1]]);
            if($request->status == 2)
                $this->worksheet = $this->worksheet->where('strategy',2)->orWhere('financial',2)->orWhere('legal',2)->orWhere('marketing',2);
            if($request->status == 3)
                $this->worksheet = $this->worksheet->where([['strategy',3],['financial',3],['legal',3],['marketing',3]]);
        }
        if(isset($request->industries)){
            $industries = explode(",", $request->industries);
            $this->worksheet = $this->worksheet->whereHas('user',function($q) use($industries){
                $q->whereIn('industry_id',$industries);
            });
        }
        if(isset($request->from)){
            $from =$request->from;
           
            $this->worksheet = $this->worksheet->whereHas('user',function($q) use($from){
                $q->where('email_verified_at','>=',$from);
            });
        }
        if(isset($request->to)){
            $to = $request->to;
            $this->worksheet = $this->worksheet->whereHas('user',function($q) use($to){
                $q->where('email_verified_at','<=',$to);
            });
        }
        return WorksheetResource::collection($this->worksheet->get());
    }

    public function pending(PendingWorksheetRequest $request)
    {
        $request = $request->validated();

        return $this->worksheet->pending($request);
    }

    public function saved(PendingWorksheetRequest $request)
    {
        $request = $request->validated();

        return $this->worksheet->savedState($request);
    }

    public function submit()
    {
        return $this->worksheet->submit();    
    }

    public function ready(Worksheet $worksheet)
    {
        return $this->worksheet->ready($worksheet);    
    }
}
