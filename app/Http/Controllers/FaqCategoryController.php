<?php

namespace App\Http\Controllers;

use App\Models\FaqCategory;
use Illuminate\Http\Request;
use App\Http\Requests\FaqCategory\StoreFaqCategoryRequest;
use App\Http\Requests\FaqCategory\UpdateFaqCategoryRequest;

class FaqCategoryController extends Controller
{
    protected $faqCategory;
    
    public function __construct(FaqCategory $faqCategory)
    {
        $this->faqCategory = $faqCategory;
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->faqCategory->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreFaqCategoryRequest $request)
    {
        $request = $request->validated();

        return $this->faqCategory->createModel($request);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FaqCategory  $faqCategory
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateFaqCategoryRequest $request)
    {
        $request = $request->validated();
        
        return $this->faqCategory->updateModel($request);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FaqCategory  $faqCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(FaqCategory $faqCategory)
    {
        $faqCategory->delete();

        return response()->json([
            "status"=>"success"
        ], 204);
    }
}
