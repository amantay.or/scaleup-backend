<?php

namespace App\Http\Controllers;

use App\Models\Question;
use App\Models\Section;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Resources\Question\QuestionCollection;
use App\Http\Resources\Question\QuestionResource;

class QuestionController extends Controller
{

    protected $question;

    protected $section;
    
    public function __construct(Question $question, Section $section)
    {
        $this->question = $question;
        $this->section = $section;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function legalIndex()
    {
        $section = $this->section->find(3);
        
        return QuestionResource::collection($section->questions);
    }

    public function strategyIndex()
    {
        $section = $this->section->find(1);
        
        
        return QuestionResource::collection($section->questions);
    }

    public function financialIndex()
    {
        $section = $this->section->find(2);
        
        return QuestionResource::collection($section->questions);
    }

    public function marketingIndex()
    {
        $section = $this->section->find(4);
        
        return QuestionResource::collection($section->questions);
    }
   



    public function legalIndexUser(User $user)
    {
       
        $section = $this->section->find(3);

        
        return QuestionResource::collection($section->questions);
    }

    public function strategyIndexUser(User $user)
    {
        $section = $this->section->find(1);
        
        
        return QuestionResource::collection($section->questions);
    }

    public function financialIndexUser(User $user)
    {
        $section = $this->section->find(2);
        
        return QuestionResource::collection($section->questions);
    }

    public function marketingIndexUser(User $user)
    {
        $section = $this->section->find(4);
        
        return QuestionResource::collection($section->questions);
    }
}
