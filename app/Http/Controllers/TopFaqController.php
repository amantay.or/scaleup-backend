<?php

namespace App\Http\Controllers;

use App\Models\TopFaq;
use Illuminate\Http\Request;
use App\Http\Requests\TopFaq\UpdateTopFaqRequest;

class TopFaqController extends Controller
{
    protected $topFaq;
    
    public function __construct(TopFaq $topFaq)
    {
        $this->topFaq = $topFaq;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->topFaq->get();
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TopFaq  $topFaq
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTopFaqRequest $request, TopFaq $topFaq)
    {
        $request = $request->validated();

        return $this->topFaq->updateModel($request,$topFaq);
    }
}
