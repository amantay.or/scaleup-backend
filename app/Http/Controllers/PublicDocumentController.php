<?php

namespace App\Http\Controllers;

use App\Models\PublicDocument;
use Illuminate\Http\Request;
use App\Http\Requests\Document\PublicDocumentRequest;
use File;
use Illuminate\Support\Facades\Storage;
use Response;
use App\Models\User;
use Log;
use ZipArchive;
//Microsoft
use Microsoft\Graph\Graph;
use League\Flysystem\Filesystem;
use NicolasBeauvais\FlysystemOneDrive\OneDriveAdapter;
use DB;
use Illuminate\Support\Str;

class PublicDocumentController extends Controller
{
   public function update(PublicDocumentRequest $request)
   {
        $request = $request->validated();
        
        $public_document = auth()->user()->public_document()->where([
            ['strategy',$request['strategy']],
            ['financial',$request['financial']],
            ['legal',$request['legal']],
            ['marketing',$request['marketing']],
        ])->first();

        return $public_document;
   }

   public function getDocuments(Request $request)
   {
        $token = $request->token;
        $public_document = PublicDocument::where('token',$token)->first();

        if(!$public_document){
            return response()->json(['status'=>'error',"message"=>'Документы не нашлись'],404); 
        }
        $user = $public_document->user;
        
        $path = 'documents/users/'.$user->id;
        
        $data = [];
        
        $data =  $this->recoursive($path);

        unset($data['file_0']);
        if($public_document->strategy == 0 ){
            unset($data['strategy']);
        }
        if($public_document->legal == 0 ){
            unset($data['law']);
        }
        if($public_document->marketing == 0 ){
            unset($data['marketing']);
        }
        if($public_document->financial == 0 ){
            unset($data['finance']);
        }

        $data = [
            'data'=>$data,
            'user'=>$user
        ];
        return $data;
   }


   public function recoursive($path)
    {
        $docs = File::files(storage_path('app/'.$path));

        $directories = Storage::directories($path);
        $data = [];
        foreach ($docs as $key => $doc) {

            $data["file_".$key] = ['name'=>$doc->getFilename(),'size'=>(int)($doc->getSize()/1024),'type'=>'file'];
        }
       
        foreach ($directories as $key => $dir) {

           
            $dir_arr = explode("/",$dir);
            $dir_name = $dir_arr[array_key_last($dir_arr)];

            
            $new_path = $path.'/'.$dir_name;
            $docs = File::files(storage_path('app/'.$new_path));

            $file_count = count(File::allFiles(storage_path('app/'.$new_path)));
            
            $directories = Storage::directories($new_path);
           
            if(!empty($docs) || !empty($directories)){
              
                $data[$dir_name] =  ['data'=>$this->recoursive($new_path),'count'=>$file_count,'type'=>'folder'];
            }
            else{
                $data[$dir_name] = ['data'=>null,'count'=>$file_count,'type'=>'folder'];
            }

        }

        return $data;
    }

    public function getFileByLink(Request $request)
    {
        $public_document = PublicDocument::where('token',$request->token)->first();
        
        if(!$public_document) {
            return response()->json(['status'=>'error',"message"=>'Документы не нашлись'],404); 
        }
        $user = $public_document->user;

        if(!$user) {
            return response()->json(['status'=>'error',"message"=>'Документы не нашлись'],404); 
        }
        
        $section = explode("/",$request->link)[0];
        
        if($public_document->{$section} == 0){
            return response()->json(['status'=>'error',"message"=>'Документы не нашлись'],404); 
        }

        $file_path = "ScaleUp"."/".$user->id.'/'.$request->link;

        $graph = new Graph();

        $onedrive = DB::table('onedrive')->first();

        $graph->setAccessToken($onedrive->access_token);

        $adapter = new OneDriveAdapter($graph, 'root');

        $filesystem = new Filesystem($adapter);

        $rawData = $filesystem->read($file_path);
        $metadata = $filesystem->getMetadata($file_path);
       
        return response($rawData, 200)
            ->header('ContentType', $metadata['mimetype'])
            ->header('Content-Disposition', "attachment; filename=".$metadata['path']);
    }

    public function getAllFilesByLink(){

        $user = auth()->user();
       
        $file_path = "/1SfQBBNzmcUrPPssDe4TBpM1c6RJUbORw";

        $users_folders = collect(Storage::disk('google')->listContents($file_path, false));

        $user_folder = $users_folders->where('filename', '=', 8)->first();

        $directories = Storage::directories('documents/templates-files');

        $zip = new ZipArchive();
        $zip->open(public_path("zip.zip"), ZipArchive::CREATE);


        $this->recoursiveDir($directories,$user_folder['path'], $zip);

        $zip->close();
    }

    public function recoursiveDir($directories,$path, $zip)
    {

        $files = collect(Storage::disk('google')->listContents($path, false));

        dd($files);

        foreach ($directories as $key => $directory) {
            
            $dir = explode("/",$directory);
            $dir = end($dir);
        }
    }
}
