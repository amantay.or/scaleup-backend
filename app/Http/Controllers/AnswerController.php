<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\User;
use App\Models\Form;
use Illuminate\Http\Request;
use App\Http\Requests\Answer\StoreAnswerRequest;
use App\Http\Requests\Answer\StoreManyAnswerRequest;
use Illuminate\Support\Facades\Storage;

class AnswerController extends Controller
{
    protected $answer;
    
    public function __construct(Answer $answer)
    {
        $this->answer = $answer;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->answer->index();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAnswerRequest $request)
    {
        $request = $request->validated();
      
        return $this->answer->storeModel($request);
    }

    public function storeMany(StoreManyAnswerRequest $request)
    {
        
        $request = $request->validated();
     
        return $this->answer->storeManyModel($request);
    }

    
    public function file(Request $request)
    {
        
        return response()->file(storage_path('app/'.$request->image));
    }

    public function storeByWorker(StoreAnswerRequest $request, User $user)
    {
        $request = $request->validated();
      
        return $this->answer->storeByWorker($request,$user);
    }

    public function storeManyByWorker(StoreManyAnswerRequest $request, User $user)
    {
        $request = $request->validated();
      
        return $this->answer->storeManyModel($request,$user);
    }
}
