<?php

namespace App\Http\Controllers;

use App\Models\Commentary;
use App\Models\Production;
use App\Models\Answer;
use Illuminate\Http\Request;
use App\Http\Resources\Commentary\CommentaryResource;
use App\Http\Requests\Commentary\StoreCommentaryRequest;
use Log;
use App\Models\Notification;

class CommentaryController extends Controller
{
    public function read(Commentary $commentary)
    {
        $commentary->read = 1;
        $commentary->save();
        $answer = Answer::find($commentary->answer_id);
        $section = $answer->section_status;
    
       $answer->update(['commentary_status'=>2]);

        $count = auth()->user()->answers()->where('section_status',$section)->where('commentary_status',1)->count();
        
        if($count == 0){
            auth()->user()->production()->update(['r_'.$section=>1,$section=>1]);
        }

        Notification::notify("Комментарий принят",$commentary->user,auth()->user());
        
        return new CommentaryResource($commentary);
    }

    public function create(StoreCommentaryRequest $request)
    {
        $request = $request->validated();

        $production = Production::where('user_id',$request['user_id'])->first();
        
        $user = auth()->user();
        $role = $user->roles[0]->name;

        if($production){
            if($role == "editor"){
                $this->updateEditor($request,$production,"r_");
            }
            else{
                $this->updateEditor($request,$production,"");
            }
        }
        
        $commentary =  $user->commentary()->updateOrCreate(
            ['answer_id'=>$request['answer_id'],'user_id'=>$user->id],
            ['text' =>$request['text']]
        );

        Notification::notify("$role оставил вам новый комментарий",$production->user,$user);

        $answer = Answer::find($request['answer_id'])->update(['disabled'=>0, 'commentary_status' => 1]);
        
        return new CommentaryResource($commentary);
    }

    public static function updateEditor($request,$production,$role){

        // 0 - К проверке
        // 1 - Проверяю 
        // 2 - Требуется исправление
        // 3 - Готово
        
        switch ($request['section']) {
            case 'strategy':
                $production->update([$role.'strategy'=>2]);
                break;
            case 'legal':
                $production->update([$role.'legal'=>2]);
                break;
            case 'financial':
                $production->update([$role.'financial'=>2]);
                break; 
            case 'marketing':
                $production->update([$role.'marketing'=>2]);
                break; 
            
        }
        return response()->json(['status'=>'success']); 
    }


    public function delete(Commentary $commentary)
    {
        $commentary->delete();

        return response()->noContent();
    }

}
