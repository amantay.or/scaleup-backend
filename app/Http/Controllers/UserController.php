<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\User\UpdateUserRequest;
use App\Http\Requests\User\UpdatePasswordRequest;
use App\Http\Requests\Worker\StoreWorkerRequest;
use App\Http\Requests\Worker\UpdateWorkerRequest;
use App\Http\Requests\User\UpdateSomeUserRequest;
use Carbon\Carbon;
use App\Http\Resources\User\UserResource;
use DB;

class UserController extends Controller
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request)
    {
        $request = $request->validated();

        return new UserResource($this->user->updateModel($request));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function updatePassword(UpdatePasswordRequest $request)
    {
        $request = $request->validated();

        return $this->user->updatePassword($request);
    }


    public function workers()
    {
        return UserResource::collection($this->user->workers());
    }

    public function createWorker(StoreWorkerRequest $request)
    {

        
        $request = $request->validated();
      
        $password = $request['password'];
        $request['company'] = "ScaleUp";
        $request['docs'] = 1;//access
        $request['email_verified_at'] = Carbon::now();
        $request['password'] = bcrypt($request['password']);
        return $this->user->createWorker($request,$password);
    }

    public function deleteWorker(User $user)
    {
        return $this->user->deleteWorker($user);
    }

    public function updateWorker(UpdateWorkerRequest $request,User $user)
    {
        $request = $request->validated($user);
        
        $request['send_passsword'] = $request['password'];
        if(isset($request['password']))
            $request['password'] = bcrypt($request['password']);
        
        return $this->user->updateWorker($request,$user);

    }

    public function list()
    {
        return UserResource::collection($this->user->list());
    }

    public function updateUser(UpdateSomeUserRequest $request,User $user)
    {
        $request = $request->validated($user);
        
        if(isset($request['password']))
            $request['password'] = bcrypt($request['password']);
      
        return new UserResource($this->user->updateUser($request,$user));
    }

    public function emptyData(User $user){

        DB::table('answers')->where('user_id',$user->id)->delete();
        DB::table('commentaries')->where('user_id',$user->id)->delete();
        DB::table('documents')->where('user_id',$user->id)->delete();
        DB::table('answers')->where('user_id',$user->id)->delete();
        DB::table('worksheets')->where('user_id',$user->id)->delete();
        DB::table('productions')->where('user_id',$user->id)->delete();
        DB::table('public_documents')->where('user_id',$user->id)->delete();

        $user->docs = 0;
        $user->save();

        $user->payment->update(['payment_status_id'=>1, 'service_id'=>4, 'payment_type_id'=>3]);

        return response()->json([
            "status"=>"success",
            "message" => "$user->fio empty"
        ], 204);

    }
    
}
