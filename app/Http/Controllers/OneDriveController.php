<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use DB;
use App\Models\Document;

class OneDriveController extends Controller
{
    public function callback(Request $request){
        $code = $request->code;

        try {
            $response = Http::withHeaders([
                'Content-Type' => 'application/x-www-form-urlencoded',
            ])
            ->asForm()
            ->post('https://login.microsoftonline.com/common/oauth2/v2.0/token', [
                
                'client_id' => '7e3c975c-8d07-4a64-8770-1b5e113119c0',
                'redirect_uri' => 'http://localhost:8000/callback',
                'client_secret'=>'ykveYR6246:_xfaDYSJT8|}',
                'code'=>$code,
                'grant_type'=>'authorization_code',
            ]);
    
            $response = $response->json();
            
            DB::table('onedrive')->where('id',1)->update(['access_token'=>$response['access_token'],'refresh_token'=>$response['refresh_token']]);
            
            return ["status"=>"success"];
        } catch (\Throwable $th) {
            dd($th);
            return ["status"=>"error"];
        }

    }

    public static function refresh(){
        try {

            $onedrive = DB::table('onedrive')->first();

            $response = Http::withHeaders([
                'Content-Type' => 'application/x-www-form-urlencoded',
            ])
            ->asForm()
            ->post('https://login.microsoftonline.com/common/oauth2/v2.0/token', [
                
                'client_id' => '7e3c975c-8d07-4a64-8770-1b5e113119c0',
                'redirect_uri' => 'http://localhost:8000/callback',
                'client_secret'=>'ykveYR6246:_xfaDYSJT8|}',
                'refresh_token'=>$onedrive->refresh_token,
                'grant_type'=>'refresh_token',
            ]);
    
            $response = $response->json();
            
            DB::table('onedrive')->where('id',1)->update(['access_token'=>$response['access_token'],'refresh_token'=>$response['refresh_token']]);
            
            return ["status"=>"success"];
        } catch (\Throwable $th) {
            dd($th);
            return ["status"=>"error"];
        }
    }

    public function test(){
        $document = new Document();

        $document->createFromLocalOneDrive(9);
    }
}
