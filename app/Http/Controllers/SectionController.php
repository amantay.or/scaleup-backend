<?php

namespace App\Http\Controllers;

use App\Models\Section;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\Section\UpdateSectionRequest;
use App\Http\Resources\Payment\PaymentResourceWithInvoice;

class SectionController extends Controller
{

    protected $section;
    
    public function __construct(Section $section)
    {
        $this->section = $section;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        $production = auth()->user()->production;
        $worksheet = auth()->user()->worksheet;
       
        $sections = $this->section->withCount('forms')->withCount('answers')->get();
        
        $types = ['strategy','financial','legal','marketing'];
        foreach ($sections as $key => $section) {

            $section->worksheet = $worksheet;
            $section->production = $production;

            if($production){
                
                $section->status = $this->setStatusProductionClient($production,$types[$key]);
            }
            else if($worksheet){
                $section->status = $this->setStatusWorksheet($worksheet,$types[$key]);
            }
        }
      
        return $sections;  
    }

    public function setStatusProductionClient($production,$type){
        if($production->{$type} == 0){
            return 'Готов';
        }
        else if($production->{$type} == 1){
            return 'Готов';
        }
        else if($production->{$type} == 2){
            return 'Требуется исправление';
        }
        return 'Готов';
    }



    public function setStatusProduction($production,$type){

        if($production->{$type} == 0){
            return 'К проверке';
        }
        else if($production->{$type} == 1){
            return 'Проверяю';
        }
        else if($production->{$type} == 2){
            return 'Требуется исправление';
        }
        return 'Готов';
    }

    public function setStatusWorksheet($worksheet,$type){

        if($worksheet->{$type} == 1){
            return 'К проверке';
        }
        else if($worksheet->{$type} == 2){
            return 'Проверяю';
        }
        else if($worksheet->{$type} == 3){
            return 'Готов';
        }
        return 'Проверяю';
    }

 
    public function indexByWorker(User $user)
    {
        
        $role = auth()->user()->roles[0]->name;
        
        $permission = $this->permission($role);

        $sections = $this->section->whereIn('id',$permission)->withCount('forms')->withCount('answers')->get();
        
        $payment = new PaymentResourceWithInvoice($user->payment);

        $production = $user->production;

        
        $types = ['strategy','financial','legal','marketing'];
        foreach ($sections as $key => $section) {
            $section->payment = $payment;
            $section->prodction = $production;
            $section->status = "не известно";
            if($production){
                $section->status = $this->setStatusProduction($production,$types[$section->id - 1]);
            }
         
        }
        return $sections;
    }

    public function indexByModerator(User $user)
    {

        $sections = $this->section->withCount('forms')->withCount('answers')->get();
        
        $payment = new PaymentResourceWithInvoice($user->payment);

        $worksheet = $user->worksheet;

        $types = ['strategy','financial','legal','marketing'];
        
        foreach ($sections as $key => $section) {
            $section->payment = $payment;
            $section->worksheet = $worksheet;
            $section->status = $this->setStatusWorksheet($worksheet,$types[$key]);
        }
        return $sections;
    }



    public function permission($role)
    {
        $permission = [];
        switch ($role) {
            case 'editor':
                $permission = [1,2,3,4];
                break;
            case 'admin':
                $permission = [1,2,3,4];
                break;
            case 'financier':
                $permission = [2];
                break; 
            case 'lawyer':
                $permission = [3];
                break; 
            case 'marketer':
                $permission = [1,4];
                break; 
            case 'moderator':
                $permission = [1,2,3,4];
                break; 
        }

        return $permission;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSectionRequest $request, Section $section)
    {
        $request = $request->validated();
        
        return $this->section->updateModel($request,$section);
        
    }

}
