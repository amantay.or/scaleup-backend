<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Notification;
use App\Models\User;

class NotificationController extends Controller
{
    public function get()
    {
        $user = auth()->user();
       
        $notifications = Notification::where('to',$user->id)->where('type',$user->roles[0]->id)->with('from')->orderByDesc('id')->take(10)->get();
        
        Notification::where('status',0)->where('to',$user->id)->where('type',$user->roles[0]->id)->update(['status'=>1]);

        return $notifications;
    }
}
