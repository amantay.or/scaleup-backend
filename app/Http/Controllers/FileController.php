<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use File;
use App\Models\User;
use App\Models\Form;
use Log;
use Response;
use App\Models\PublicDocument;

//Microsoft
use Microsoft\Graph\Graph;
use League\Flysystem\Filesystem;
use NicolasBeauvais\FlysystemOneDrive\OneDriveAdapter;
use DB;

class FileController extends Controller
{

    protected $forms;

    public static function createUserFiles(User $user)
    {
        $this->forms = Form::with('answer')->get()->keyBy('key');

        return $this->createUserFolders($user->id);
    }

    public function getUserFiles(User $user)
    {
        $auth = auth()->user();
        if($auth->roles[0]->name == "client"){
            if($auth->docs == 0){
                return response()->json(['status'=>'error','message'=>'Ваши документы еще не готовы!'],404); 
            }
            if($user->id != $auth->id){
                return response()->json(['status'=>'error','message'=>'Вы не можете просматривать чужие документы!'],403); 
            }
        }
        
        $path = 'documents/users/'.$user->id;
        
        $data = [];
        
        $data =  $this->recoursive($path);
        
        $role = auth()->user()->roles[0]->name;

        unset($data['file_0']);
        
        if($role == "financier" ){
            unset($data['law']);
            unset($data['marketing']);
            unset($data['strategy']);
        }
        else if($role == "lawyer"){
            unset($data['finance']);
            unset($data['marketing']);
            unset($data['strategy']);
        }  
        else if($role == "marketer"){
            unset($data['law']);
            unset($data['finance']);
        }     
     
        return $data;

    }

    public function recoursive($path)
    {
        
        $docs = File::files(storage_path('app/'.$path));

        $directories = Storage::directories($path);
        $data = [];
        foreach ($docs as $key => $doc) {

            $data["file_".$key] = ['name'=>$doc->getFilename(),'size'=>(int)($doc->getSize()/1024),'type'=>'file'];
        }

        
       
        foreach ($directories as $key => $dir) {

           
            $dir_arr = explode("/",$dir);
            $dir_name = $dir_arr[array_key_last($dir_arr)];

            
            $new_path = $path.'/'.$dir_name;
            $docs = File::files(storage_path('app/'.$new_path));

            $file_count = count(File::allFiles(storage_path('app/'.$new_path)));
            
            $directories = Storage::directories($new_path);
           
            if(!empty($docs) || !empty($directories)){
              
                $data[$dir_name] =  ['data'=>$this->recoursive($new_path),'count'=>$file_count,'type'=>'folder'];
            }
            else{
                $data[$dir_name] = ['data'=>null,'count'=>$file_count,'type'=>'folder'];
            }

        }

        return $data;
       
    }

    public function getFileByLink(Request $request, User $user)
    {
       
        $path = storage_path().'/app/documents/users/'.$user->id.'/'.$request->link;
        
      
        return Response::download($path);
    }

    public function getGoogleFileByLinkPublic(Request $request, $token){

        $public_document = PublicDocument::where('token',$token)->first();

        if(!$public_document){
            return response()->json(['status'=>'error',"message"=>'Документы не нашлись'],404); 
        }
        $user = $public_document->user;

        $file_path = "ScaleUp"."/".$user->id.'/'.$request->link;

        $graph = new Graph();

        $onedrive = DB::table('onedrive')->first();

        $graph->setAccessToken($onedrive->access_token);

        $adapter = new OneDriveAdapter($graph, 'root');

        $filesystem = new Filesystem($adapter);

        $rawData = $filesystem->read($file_path);
        $metadata = $filesystem->getMetadata($file_path);
       
        return response($rawData, 200)
            ->header('ContentType', $metadata['mimetype'])
            ->header('Content-Disposition', "attachment; filename=".$metadata['path']);

        return $rawData;
    }

    public function getGoogleFileByLink(Request $request, User $user)
    {   
        $file_path = "ScaleUp"."/".$user->id.'/'.$request->link;

        $graph = new Graph();

        $onedrive = DB::table('onedrive')->first();

        $graph->setAccessToken($onedrive->access_token);

        $adapter = new OneDriveAdapter($graph, 'root');

        $filesystem = new Filesystem($adapter);

        $rawData = $filesystem->read($file_path);
        $metadata = $filesystem->getMetadata($file_path);
       
        return response($rawData, 200)
            ->header('ContentType', $metadata['mimetype'])
            ->header('Content-Disposition', "attachment; filename=".$metadata['path']);

        return $rawData;
    }

    public function openGoogleZip(User $user){
        $file_path = "ScaleUp"."/".$user->id.'/archive.zip';

        $graph = new Graph();

        $onedrive = DB::table('onedrive')->first();

        $graph->setAccessToken($onedrive->access_token);

        $adapter = new OneDriveAdapter($graph, 'root');

        $filesystem = new Filesystem($adapter);

        $rawData = $filesystem->read($file_path);
        $metadata = $filesystem->getMetadata($file_path);
       
        return response($rawData, 200)
            ->header('ContentType', $metadata['mimetype'])
            ->header('Content-Disposition', "attachment; filename=".$metadata['path']);

        return $rawData;
    }

    public function openGoogleFileByLink(Request $request, User $user){
       
        $file_path = "ScaleUp"."/".$user->id.'/'.$request->link;

        $graph = new Graph();

        $onedrive = DB::table('onedrive')->first();

        $graph->setAccessToken($onedrive->access_token);

        $adapter = new OneDriveAdapter($graph, 'root');

        $filesystem = new Filesystem($adapter);

        $metadata = $filesystem->getMetadata($file_path);

        return response()->json(['status'=>'success','link'=>$metadata['link']]);
    }

   


    public function createUserFolders($username)
    {
        $path = 'documents/users/'.$username;
        $template = 'documents/templates-files';
      
       
        Storage::disk('local')->makeDirectory($path);
    
        File::copyDirectory(storage_path('app/'.$template), storage_path('app/'.$path));

        return $this->getDocs($username);

    }

    public function getDocs($username)
    {
        
        $path = 'documents/templates';
       
        $docs = File::allFiles(storage_path('app/'.$path));

       

        foreach ($docs as $key => $doc) {

            $doc = $doc->getRelativePathname();
            
            
            
            $path = 'documents/templates/'.$doc;

            $phpword = new \PhpOffice\PhpWord\TemplateProcessor(storage_path('app/'.$path));
                   
            $phpword = $this->fillDocuments($phpword,'legal',1,121);
            $phpword = $this->fillDocuments($phpword,'strategy',1,108);
            $phpword = $this->fillDocuments($phpword,'financial',1,98);
            $phpword = $this->fillDocuments($phpword,'marketing',1,38);


            if(strpos($doc, "docx")){
                $doc = explode(".docx", $doc)[0] . '.doc';
            }

            $path = 'documents/users/'.$username.'/'.$doc;

           
            $phpword->saveAs(storage_path('app/'.$path));

        }
        return response()->json(['status'=>'success','message'=>'OK']);
    }

    public function fillDocuments($phpword,$section,$start,$finish){

       

        for ($i=$start; $i <= $finish; $i++) { 

            $key = $section.'-'.$i;
            if($this->forms[$key]->answer){
                if($this->forms[$key]->type == 1 || $this->forms[$key]->type == 2)  
				{
					$ans = $this->forms[$key]->answer->answers;
					if(!$ans)
                        $ans = "";
                    else{
                        foreach($ans as $key2=>$value){
                            $answer = $value;
                        }
                        $ans = $answer;
                    }
			
                    $phpword->setValue($key, $ans);
				}
                else if($this->forms[$key]->type == 4){

                    $answers = $this->forms[$key]->answer->answers;
                    
					if(!$answers)
                        $answers = [];
                    
                    $answers = $this->getValuesOfCheckbox($answers);

                    $phpword->setValue($key, $answers);
                }
                else{

                    
                    $answers = $this->forms[$key]->answer->answers;
                    if(!$answers){
                        $phpword->setValue($key, ' ');
                    }
                    else{
                        $answers = $answers[0];
                        
                        $image_path = Storage::path($answers);
                        
                        $phpword->setImageValue($key, array(
                            'src'  => $image_path,
                            'size' => array( 500,500 ) //px
                        ));
                    }  
                }
            }
            else {
                $phpword->setValue($key, ' ');
            }   
        }
        return $phpword;
    }

    public function getValuesOfCheckbox($answers){

        $ans = "";
        foreach ($answers as $key => $answer) {
          
            $ans.= "- ".$answer;
            $ans.="</w:t><w:br/><w:t>";
        }
        return $ans;
    }

    public function uploadFileToUser(Request $request, User $user){

       
        $document = $user->document;
        
        $path = 'documents/users/'.$user->id.'/'.$request['link'].'/';

        $path_to_google_disk = $user->id.'/'.$request['link'];

        $graph = new Graph();

        $onedrive = DB::table('onedrive')->first();

        $graph->setAccessToken($onedrive->access_token);

        $adapter = new OneDriveAdapter($graph, 'root');

        $filesystem = new Filesystem($adapter);
        
        if($document){

            $google_path = "ScaleUp";

            $filesystem->put($google_path.'/'.$path_to_google_disk.'/'.$request['file']->getClientOriginalName(),file_get_contents($request['file']));
        }
        
        $request['file']->storeAs($path,$request['file']->getClientOriginalName());

        return $this->getUserFiles($user);

    }

    public function deleteFileToUser(Request $request, User $user){

        $path = storage_path().'/app/documents/users/'.$user->id.'/'.$request['link'];

        if(File::exists($path)){
           
            unlink($path);
        }
        $graph = new Graph();

        $onedrive = DB::table('onedrive')->first();

        $graph->setAccessToken($onedrive->access_token);

        $adapter = new OneDriveAdapter($graph, 'root');

        $filesystem = new Filesystem($adapter);

        $filesystem->delete("ScaleUp/".$user->id.'/'.$request['link']);

        return $this->getUserFiles($user);
    }


    public function getTemplates1()
    {
        
        $path = 'documents/templates1';
        
        $data = [];
        
        $data =  $this->recoursive($path);
     
        return $data;

    }

    public function getTemplates2()
    {
        
        $path = 'documents/templates2';
        
        $data = [];
        
        $data =  $this->recoursive($path);
     
        return $data;

    }

    public function getTemplates3()
    {
        
        $path = 'documents/templates3';
        
        $data = [];
        
        $data =  $this->recoursive($path);
     
        return $data;

    }
    
    public function getFileByLinkTemplate1(Request $request)
    {
       
        $path = storage_path().'/app/documents/templates1/'.$request->link;
        
      
        return Response::download($path);
    }

    public function getFileByLinkTemplate2(Request $request)
    {
       
        $path = storage_path().'/app/documents/templates2/'.$request->link;
        
      
        return Response::download($path);
    }

    public function getFileByLinkTemplate3(Request $request)
    {
       
        $path = storage_path().'/app/documents/templates3/'.$request->link;
        
      
        return Response::download($path);
    }


    public function uploadFileTemplate1(Request $request){

        $path = 'documents/templates1/'.$request['link'].'/';

        $request['file']->storeAs($path,$request['file']->getClientOriginalName());

        return $this->getTemplates1();

    }

    public function uploadFileTemplate2(Request $request){

        $path = 'documents/templates2/'.$request['link'].'/';

        $request['file']->storeAs($path,$request['file']->getClientOriginalName());

        return $this->getTemplates2();

    }

    public function uploadFileTemplate3(Request $request){

        $path = 'documents/templates3/'.$request['link'].'/';

        $request['file']->storeAs($path,$request['file']->getClientOriginalName());

        return $this->getTemplates3();

    }

    public function uploadFolderTemplate1(Request $request){

        $path1 = 'app/documents/templates1/'.$request['link'].'/'.$request['folder'];
        $path2 = 'app/documents/templates-files1/'.$request['link'].'/'.$request['folder'];
        
        File::makeDirectory(storage_path($path1), 0777, true, true);
        File::makeDirectory(storage_path($path2), 0777, true, true);
    //    Storage::makeDirectory($path1);
    //    Storage::makeDirectory($path2);

        return $this->getTemplates1();

    }

    public function uploadFolderTemplate2(Request $request){

        $path1 = 'app/documents/templates2/'.$request['link'].'/'.$request['folder'];
        $path2 = 'app/documents/templates-files2/'.$request['link'].'/'.$request['folder'];
        
        File::makeDirectory(storage_path($path1), 0777, true, true);
        File::makeDirectory(storage_path($path2), 0777, true, true);
    //    Storage::makeDirectory($path1);
    //    Storage::makeDirectory($path2);

        return $this->getTemplates2();

    }

    public function uploadFolderTemplate3(Request $request){

        $path1 = 'app/documents/templates3/'.$request['link'].'/'.$request['folder'];
        $path2 = 'app/documents/templates-files3/'.$request['link'].'/'.$request['folder'];
        
        File::makeDirectory(storage_path($path1), 0777, true, true);
        File::makeDirectory(storage_path($path2), 0777, true, true);
    //    Storage::makeDirectory($path1);
    //    Storage::makeDirectory($path2);

        return $this->getTemplates3();

    }

    public function uploadFolderToUser(Request $request, User $user){

        $path1 = 'app/documents/users/'.$user->id.'/'.$request['link'].'/'.$request['folder'];
        $path2 = 'ScaleUp/'.$user->id.'/'.$request['link'].'/'.$request['folder'];

     
        File::makeDirectory(storage_path($path1), 0777, true, true);

        $graph = new Graph();

        $onedrive = DB::table('onedrive')->first();

        $graph->setAccessToken($onedrive->access_token);

        $adapter = new OneDriveAdapter($graph, 'root');

        $filesystem = new Filesystem($adapter);

        $filesystem->createDir($path2);

    //    Storage::makeDirectory($path1);
    //    Storage::makeDirectory($path2);

        return $this->getUserFiles($user);

    }

    public function deleteFileTemplate1(Request $request){

        $path = storage_path().'/app/documents/templates1/'.$request['link'];

        if(File::exists($path)){
           
            unlink($path);
        }
        
        return $this->getTemplates1();
    }

    public function deleteFileTemplate2(Request $request){

        $path = storage_path().'/app/documents/templates2/'.$request['link'];

        if(File::exists($path)){
           
            unlink($path);
        }
        
        return $this->getTemplates2();
    }

    public function deleteFileTemplate3(Request $request){

        $path = storage_path().'/app/documents/templates3/'.$request['link'];

        if(File::exists($path)){
           
            unlink($path);
        }
        
        return $this->getTemplates3();
    }

    public function deleteFolderTemplate1(Request $request){

        $path1 = 'documents/templates1/'.$request['link'];
        $path2 = 'documents/templates-files1/'.$request['link'];
       
       
        if(empty($request['link']) || $request['link'] == "finance" || $request['link'] == "law" || $request['link'] == "marketing" || $request['link'] == "strategy"){
            return $this->getTemplates1();
        }
        
        Storage::deleteDirectory($path1);
        Storage::deleteDirectory($path2);

        return $this->getTemplates1();

    }

    public function deleteFolderTemplate2(Request $request){

        $path1 = 'documents/templates2/'.$request['link'];
        $path2 = 'documents/templates-files2/'.$request['link'];
       
       
        if(empty($request['link']) || $request['link'] == "finance" || $request['link'] == "law" || $request['link'] == "marketing" || $request['link'] == "strategy"){
            return $this->getTemplates2();
        }
        
        Storage::deleteDirectory($path1);
        Storage::deleteDirectory($path2);

        return $this->getTemplates2();

    }

    public function deleteFolderTemplate3(Request $request){

        $path1 = 'documents/templates3/'.$request['link'];
        $path2 = 'documents/templates-files3/'.$request['link'];
       
       
        if(empty($request['link']) || $request['link'] == "finance" || $request['link'] == "law" || $request['link'] == "marketing" || $request['link'] == "strategy"){
            return $this->getTemplates3();
        }
        
        Storage::deleteDirectory($path1);
        Storage::deleteDirectory($path2);

        return $this->getTemplates3();

    }

    public function deleteFolderToUser(Request $request, User $user){

        $path1 = 'app/documents/users/'.$user->id.'/'.$request['link'];
        $path2 = 'ScaleUp/'.$user->id.'/'.$request['link'];
       
       
        if(empty($request['link']) || $request['link'] == "finance" || $request['link'] == "law" || $request['link'] == "marketing" || $request['link'] == "strategy"){
            return $this->getUserFiles($user);
        }
        
        File::deleteDirectory(storage_path($path1));

        $graph = new Graph();

        $onedrive = DB::table('onedrive')->first();

        $graph->setAccessToken($onedrive->access_token);

        $adapter = new OneDriveAdapter($graph, 'root');

        $filesystem = new Filesystem($adapter);

        $filesystem->deleteDir($path2);

        return $this->getUserFiles($user);

    }

    public function getAnswerFile(Request $request){
        $link = $request->link;
        
        return Storage::download($link);
    }

    public function openOneDriveFileByLink(User $user, Request $request){

        
        $endpoint = "https://graph.microsoft.com/v1.0/me/drive/items/root:/ScaleUp/".$user->id.'/'.$request['link'].":/createLink";
       
        $onedrive = DB::table('onedrive')->first();

        try {
          
            $client = new \GuzzleHttp\Client();
        
            $response = $client->request('POST', $endpoint, [
                'headers' => [
                    'Authorization' => 'Bearer '.$onedrive->access_token,
                    'Content-type' => 'application/json'
                ],
                'body' => json_encode(['type'=>'embed'])
            ]);

            $statusCode = $response->getStatusCode();
            $content = $response->getBody();

            $url = json_decode($content, true)['link']['webUrl'];

            return response()->json(['status'=>'success','link'=>$url]);
        } catch (\Throwable $th) {
            return response()->json(['status'=>'error','message' => 'Not Found!'], 404);
        }
        
    }




}
