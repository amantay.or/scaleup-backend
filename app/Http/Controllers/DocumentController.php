<?php

namespace App\Http\Controllers;

use App\Models\Document;
use Illuminate\Http\Request;
use App\Http\Requests\Production\UpdateProductionRequest;
use App\Models\PublicDocument;
use App\Models\User;
use Illuminate\Support\Str;
use App\Models\Notification;
use App\Jobs\ZipRender;

class DocumentController extends Controller
{
    protected $document;

    public function __construct(Document $document)
    {
        $this->document = $document;
    }
    public function index(Request $request)
    {
        if(isset($request->status)){
            if($request->status == 0)
                $this->document = $this->document->where([['strategy',0],['financial',0],['legal',0],['marketing',0],['r_strategy',0],['r_financial',0],['r_legal',0],['r_marketing',0]]);
            else if($request->status == 1)
                $this->document = $this->document->where('strategy',0)->orWhere('financial',0)->orWhere('legal',0)->orWhere('marketing',0)
                    ->orWhere('r_strategy',0)->orWhere('r_financial',0)->orWhere('r_legal',0)->orWhere('r_marketing',0);
            else if($request->status == 3)
                $this->document = $this->document->where([['strategy',1],['financial',1],['legal',1],['marketing',1],['r_strategy',1],['r_financial',1],['r_legal',1],['r_marketing',1]]);
        }
        
        if(isset($request->industries)){

            $industries = explode(",", $request->industries);
            
            $this->document = $this->document->whereHas('user',function($q) use($industries){
                $q->whereIn('industry_id',$industries);
            });
        }
        if(isset($request->from)){
            $from =$request->from;
           
            $this->document = $this->document->whereHas('user',function($q) use($from){
                $q->where('email_verified_at','>=',$from);
            });
        }
        if(isset($request->to)){
            $to = $request->to;
            $this->document = $this->document->whereHas('user',function($q) use($to){
                $q->where('email_verified_at','<=',$to);
            });
        }

        $documents = $this->document->where('status',0)->with('user')->get();

        return $documents;
    }

    public function indexReady(Request $request)
    {
        if(isset($request->status)){
            if($request->status == 0)
                $this->document = $this->document->where([['strategy',0],['financial',0],['legal',0],['marketing',0],['r_strategy',0],['r_financial',0],['r_legal',0],['r_marketing',0]]);
            else if($request->status == 1)
                $this->document = $this->document->where('strategy',0)->orWhere('financial',0)->orWhere('legal',0)->orWhere('marketing',0)
                    ->orWhere('r_strategy',0)->orWhere('r_financial',0)->orWhere('r_legal',0)->orWhere('r_marketing',0);
            else if($request->status == 3)
                $this->document = $this->document->where([['strategy',1],['financial',1],['legal',1],['marketing',1],['r_strategy',1],['r_financial',1],['r_legal',1],['r_marketing',1]]);
        }
        
        if(isset($request->industries)){

            $industries = explode(",", $request->industries);
            
            $this->document = $this->document->whereHas('user',function($q) use($industries){
                $q->whereIn('industry_id',$industries);
            });
        }
        if(isset($request->from)){
            $from =$request->from;
           
            $this->document = $this->document->whereHas('user',function($q) use($from){
                $q->where('email_verified_at','>=',$from);
            });
        }
        if(isset($request->to)){
            $to = $request->to;
            $this->document = $this->document->whereHas('user',function($q) use($to){
                $q->where('email_verified_at','<=',$to);
            });
        }
        $documents = $this->document->where('status',1)->with('user')->get();

        return $documents;
    }
    
    public function update(Document $document)
    {
        if($document->status == 1){
            return response()->json(['status'=>'error',"message"=>'Документы уже сохранены'],403);
        }
        $role = auth()->user()->roles[0]->name;

        if($role == "editor"){
            $document->update(['r_strategy'=>1,'r_financial'=>1,'r_legal'=>1,'r_marketing'=>1]);
            
            $this->notify($document);
            return response()->json(['status'=>'success']); 
        }
        else if($role == "financier") {
            $document->update(['financial'=>1]);
            $this->notify($document);
            return response()->json(['status'=>'success']); 
        }
        else if($role == "lawyer") {
            $document->update(['legal'=>1]);
            $this->notify($document);
            return response()->json(['status'=>'success']); 
        }
        else if($role == "marketer") {
            $document->update(['strategy'=>1,'marketing'=>1]);
            $this->notify($document);
            return response()->json(['status'=>'success']); 
        }
        else if($role == "moderator" || $role == "admin"){
            if( $document->strategy == 1 && $document->financial == 1 &&
                $document->legal == 1 && $document->marketing == 1 &&
                $document->r_strategy == 1 && $document->r_financial == 1 &&
                $document->r_legal == 1 && $document->r_marketing == 1 && 
                $document->status == 0){

                    $docJob = new ZipRender($document);
                    dispatch($docJob);

                    return response()->json(['status'=>'success',"message"=>"Успешно"]); 

            }
            return response()->json(['status'=>'error',"message"=>'Сперва все документы должны быть готовы'],403); 

        }
        return response()->json(['status'=>'error',"message"=>'У вас не хватает разрешения для этого действия'],403); 
        
    }

    public function notify($document){
        if( $document->strategy == 1 && $document->financial == 1 &&
            $document->legal == 1 && $document->marketing == 1 &&
            $document->r_strategy == 1 && $document->r_financial == 1 &&
            $document->r_legal == 1 && $document->r_marketing == 1)
            {
                $company = $document->user->company;
                $users = User::whereHas("roles", function($q){ $q->where("name", "moderator")->orWhere('name','admin'); })->get();
                foreach ($users as $key => $user) {
                    Notification::notify("Документы $company готовы к отправке",$user);
                }
            }
    }

}
