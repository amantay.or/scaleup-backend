<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Service;
use App\Models\Industry;
use DB;

class DashboardController extends Controller
{
    public function index(){

        $registred = User::whereHas('roles', function($q){
            $q->where('name','client');
        })->count();

        $registred_last_month = User::whereHas('roles', function($q){
            $q->where('name','client');
        })->whereDate('created_at', '>', \Carbon\Carbon::now()->subMonth())->count();

        $ready = User::whereHas('roles', function($q){
            $q->where('name','client');
        })->where('docs',1)->count();

        $ready = User::whereHas('roles', function($q){
            $q->where('name','client');
        })->where('docs',1)->count();

        $not_ready = User::whereHas('roles', function($q){
            $q->where('name','client');
        })->whereHas('payment', function($q){
            $q->where('payment_status_id',3);
        })->where('docs',0)->count();

        $payment_waiting = User::whereHas('roles', function($q){
            $q->where('name','client');
        })->whereHas('payment', function($q){
            $q->whereIn('payment_status_id',[1,2]);
        })->count();

        $payment_types = Service::select('name')->where('type','!=',-1)->withCount('users')->get();

        $industries = Industry::select(['name'])->withCount('users')->get();

        return response()->json([
            'registred'=>$registred,
            'registred_last_month'=>$registred_last_month,
            'ready'=>$ready,
            'not_ready'=>$not_ready,
            'payment_waiting'=>$payment_waiting,
            'payment_types'=>$payment_types,
            'industries' => $industries
        ]); 
    }

    public function store(Request $request){
        
        $fileName = $request->file->getClientOriginalName();
        if($request->type == "invoice"){
            DB::table('files')->where('id',1)->update(['name'=>'preview/'.$fileName]);
        }
        if($request->type == "agreeScaleUp"){
            DB::table('files')->where('id',2)->update(['name'=>'preview/'.$fileName]);
        }
        if($request->type == "useScaleUp"){
            DB::table('files')->where('id',3)->update(['name'=>'preview/'.$fileName]);
        }
        $request->file->move('preview/',$fileName);
    }
    public function getPreviewFiles(){
        
        $files = DB::table('files')->take(3)->get();

        $files[0]->type = "invoice"; 
        $files[1]->type = "agreeScaleUp";
        $files[2]->type = "useScaleUp";

        return $files;
    }
}
