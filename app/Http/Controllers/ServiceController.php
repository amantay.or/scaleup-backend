<?php

namespace App\Http\Controllers;

use App\Models\Service;
use Illuminate\Http\Request;
use App\Http\Requests\Service\UpdateServiceRequest;

class ServiceController extends Controller
{
    protected $service;
    
    public function __construct(Service $service)
    {
        $this->service = $service;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = $this->service->take(3)->get();
        foreach ($services as $key => $service) {
            $service->data = json_decode($service->data);
        }
        
        return $services;
    }

    public function update(UpdateServiceRequest $request, Service $service)
    {
        $request = $request->validated();
        $service = $this->service->updateModel($request,$service);
        return $service;
            
    }

}
