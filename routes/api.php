<?php


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
//Controllers
use App\Http\Controllers\DocsController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\IndustryController;
use App\Http\Controllers\BlockController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\TopFaqController;
use App\Http\Controllers\FaqCategoryController;
use App\Http\Controllers\FaqController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\SectionController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\QuestionController;
use App\Http\Controllers\AnswerController;
use App\Http\Controllers\WorksheetController;
use App\Http\Controllers\ProductionController;
use App\Http\Controllers\DocumentController;
use App\Http\Controllers\CommentaryController;
use App\Http\Controllers\FileController;
use App\Http\Controllers\PublicDocumentController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\FormController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\OneDriveController;
//Models
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Users
Route::post('/register',[AuthController::class,'register']);
Route::post('/login',[AuthController::class,'login']);
Route::post('/forgot',[AuthController::class,'forgot']);
Route::post('password/reset', [AuthController::class,'reset'])->name('reset');
Route::get('password/reset',[AuthController::class,'reset_password'])->name('password.reset');
Route::get('email/verify/{id}', [AuthController::class,'verify'])->name('verification.verify');
Route::post('/check/login',[AuthController::class,'checkLogin']);

 //Block
Route::group(['prefix' => 'block'], function () {
    Route::get('/',[BlockController::class,'showFirst'])->name('block');
    Route::post('/',[BlockController::class,'update'])->name('update_block');
});

//Services
Route::group(['prefix' => 'services'], function () {
    Route::get('/',[ServiceController::class,'index'])->name('services');
});

//Top FAQ
Route::group(['prefix' => 'top-faqs'], function () {
    Route::get('/',[TopFaqController::class,'index'])->name('topFaqs');
});

//FAQ
Route::group(['prefix' => 'faqs'], function () {
    Route::get('/',[FaqController::class,'index'])->name('faqs');
});
//Invoice
Route::get('payments/invoice-file',[PaymentController::class,'getInvoiceFile']);

//FAQ Categories
Route::group(['prefix' => 'faq-categories'], function () {
    Route::get('/',[FaqCategoryController::class,'index'])->name('faqCategories');
});

Route::middleware(['auth:api','verified'])->group(function(){

    Route::get('notifications',[NotificationController::class,'get']);

    Route::get('get-answer-file',[FileController::class,'getAnswerFile']); //copy

    //User
    Route::group(['prefix' => 'users'], function () {
        Route::get('/get/{user}',[AuthController::class,'showUser']);
        Route::get('/me',[AuthController::class,'showMe']);
        Route::post('/avatar/save',[AuthController::class,'saveAvatar']);
        Route::get('/payments',[PaymentController::class,'getPayment']);
        Route::put('update/me',[UserController::class,'update']);
        Route::put('update/password',[UserController::class,'updatePassword']);
    });
   

    //Payment
    Route::group(['prefix' => 'payments'], function () {

        Route::post('/bank',[PaymentController::class,'bank']);
        Route::post('/',[PaymentController::class,'store'])->name('create_payment');
      //  Route::get('/invoice-file',[PaymentController::class,'getInvoiceFile']);
        Route::post('/invoice',[PaymentController::class,'invoice']);
        Route::post('/select',[PaymentController::class,'select']);
        Route::get('/invoice/download',[PaymentController::class,'downloadInvoice']);
        Route::group(['middleware' => ['role:moderator|admin|sale_department']], function () {
            Route::get('/accepted/{user}',[PaymentController::class,'accepted']);
            Route::get('/list',[PaymentController::class,'list']);
        });
        Route::get('/show/{user}',[PaymentController::class,'show']);
    });

    //Worksheet Client
    Route::group(['prefix' => 'worksheets','middleware' => ['role:client']], function () {
    
        Route::put('/pending',[WorksheetController::class,'pending'])->name('worksheets_pending');
        Route::put('/saved',[WorksheetController::class,'saved'])->name('worksheets_saved');
        Route::put('/submit',[WorksheetController::class,'submit'])->name('worksheets_submit');
    });

    //Worksheet Admin,Moderator
    Route::group(['prefix' => 'worksheets','middleware' => ['role:moderator|admin']], function () {
        Route::get('/',[WorksheetController::class,'index'])->name('worksheets');
        Route::put('/ready/{worksheet}',[WorksheetController::class,'ready'])->name('worksheets_ready');
    });


     
    
    //CLIENT
    Route::group([], function () {

        //Section
        Route::group(['prefix' => 'sections'], function () {
            Route::get('/',[SectionController::class,'index'])->name('sections');
            Route::get('/{user}',[SectionController::class,'indexByWorker'])->name('sections')->middleware('role:admin|editor|financier|lawyer|marketer|moderator');
            Route::get('/moderator/{user}',[SectionController::class,'indexByModerator'])->name('sections')->middleware('role:moderator|admin');
            Route::put('/{section}',[SectionController::class,'update'])->name('update_section');
        });

        //Questions
        Route::group(['prefix' => 'questions'], function () {
            Route::get('/legal',[QuestionController::class,'legalIndex'])->name('legals');
            Route::get('/strategy',[QuestionController::class,'strategyIndex'])->name('strategies');
            Route::get('/financial',[QuestionController::class,'financialIndex'])->name('financials');
            Route::get('/marketing',[QuestionController::class,'marketingIndex'])->name('marketing');
        });

        //Questions WORKER
        Route::group(['prefix' => 'questions'], function () {
            Route::get('/legal/{user}',[QuestionController::class,'legalIndexUser'])->name('legals');
            Route::get('/strategy/{user}',[QuestionController::class,'strategyIndexUser'])->name('strategies');
            Route::get('/financial/{user}',[QuestionController::class,'financialIndexUser'])->name('financials');
            Route::get('/marketing/{user}',[QuestionController::class,'marketingIndexUser'])->name('marketing');
        });

        //Answers
        Route::group(['prefix' => 'answers'], function () {
            Route::post('/',[AnswerController::class,'store'])->name('create_answer');
            Route::post('/many',[AnswerController::class,'storeMany'])->name('create_answers')->middleware('role:client');;
            Route::post('/{user}',[AnswerController::class,'storeByWorker'])->name('create_answer_by_worker');
            Route::post('/many/{user}',[AnswerController::class,'storeManyByWorker'])->name('create_answers_by_worker')->middleware('role:moderator|admin');
            Route::get('/',[AnswerController::class,'index'])->name('answer');
            Route::post('/files',[AnswerController::class,'file'])->name('files');
          
        });

        //Commentary
        Route::group(['prefix' => 'commentary'], function () {
            Route::post('/{commentary}/read',[CommentaryController::class,'read'])->name('read_commentary');
         
        });
        //Commentary WORKER
        Route::group(['prefix' => 'commentary'], function () {
            Route::post('/',[CommentaryController::class,'create'])->name('create_commentary');
            Route::delete('/{commentary}',[CommentaryController::class,'delete'])->name('delete_commentary');
         
        });

    });

    Route::group(['prefix' => 'productions','middleware' => ['role:admin|editor|financier|lawyer|marketer|moderator']], function () {
        Route::get('/',[ProductionController::class,'index'])->name('productions');
        Route::put('/{production}',[ProductionController::class,'update'])->name('update_productions');
        Route::get('/ready/{production}',[ProductionController::class,'ready'])->name('update_productions');
    });

    Route::group(['prefix' => 'documents','middleware' => ['role:admin|editor|financier|lawyer|marketer|moderator']], function () {
        Route::get('/',[DocumentController::class,'index'])->name('documents');
        Route::get('/ready',[DocumentController::class,'indexReady'])->name('documentsReady');
        Route::post('/{document}',[DocumentController::class,'update'])->name('update_documents');
      
    });
    
    Route::get('preview-files', [DashboardController::class,'getPreviewFiles']);
    

    //ADMIN
    Route::group(['middleware' => ['role:admin']], function () {
        //Workers
        Route::group(['prefix' => 'workers'], function () {
            Route::get('/',[UserController::class,'workers'])->name('workers');
            Route::post('/',[UserController::class,'createWorker'])->name('create_workers');
            Route::put('/{user}',[UserController::class,'updateWorker'])->name('update_workers');
            Route::delete('/{user}',[UserController::class,'deleteWorker'])->name('delete_workers');
        });

        //List
        Route::group(['prefix' => 'users'], function () {
            Route::get('/list',[UserController::class,'list'])->name('list');
            Route::put('/{user}',[UserController::class,'updateUser'])->name('update_user');
        });



         //Roles
        Route::group(['prefix' => 'roles'], function () {

            Route::get('/',[RoleController::class,'index'])->name('roles');
            Route::post('/',[RoleController::class,'store'])->name('create_role');
            Route::delete('/{role}',[RoleController::class,'destroy'])->name('delete_role');
            Route::post('/addPermission',[RoleController::class,'permissionToRole'])->name('permission_to_role');
        });

        //Permissions
        Route::group(['prefix' => 'permissions'], function () {
            Route::get('/',[PermissionController::class,'index'])->name('permissions');
            Route::post('/',[PermissionController::class,'store'])->name('create_permission');
            Route::delete('/{permission}',[PermissionController::class,'destroy'])->name('delete_permission');
        });

        //Industries
        Route::group(['prefix' => 'industries'], function () {
            Route::get('/',[IndustryController::class,'index'])->name('industries');
            Route::post('/',[IndustryController::class,'store'])->name('create_industry');
            Route::delete('/{industry}',[IndustryController::class,'destroy'])->name('delete_industry');
        });

            
        //Services
        Route::group(['prefix' => 'services'], function () {
            Route::put('/{service}',[ServiceController::class,'update'])->name('update_service');
            
        });

        //Top FAQ
        Route::group(['prefix' => 'top-faqs'], function () {
            Route::put('/{topFaq}',[TopFaqController::class,'update'])->name('update_topFaqs');
            
        });

        //FAQ Categories
        Route::group(['prefix' => 'faq-categories'], function () {
            Route::post('/',[FaqCategoryController::class,'store'])->name('create_faqCategory');
            Route::put('/update',[FaqCategoryController::class,'update'])->name('update_faqCategory');
            Route::delete('/{faqCategory}',[FaqCategoryController::class,'destroy'])->name('delete_faqCategory');
        });

        //FAQ
        Route::group(['prefix' => 'faqs'], function () {
            Route::post('/faq-category/{faqCategory}',[FaqController::class,'store'])->name('create_faq');
            Route::put('/update',[FaqController::class,'update'])->name('update_faq');
            Route::delete('/{faq}',[FaqController::class,'destroy'])->name('delete_faq');
        });

        //Dashboard
        Route::group(['prefix' => 'dashboard'], function () {
            Route::get('/',[DashboardController::class,'index']);
          
        });

        Route::post('preview-files', [DashboardController::class,'store']);
        

        //Anketa
        Route::group(['prefix' => 'anketa'], function () {
            Route::get('/',[FormController::class,'index']);
            Route::get('/{section}',[FormController::class,'indexSection']);
            Route::put('/update',[FormController::class,'update']);
          
        });

    });


    //DOCS
    Route::group(['prefix' => 'docs'], function () {
       
        Route::get('/create-user-files/{user}',[FileController::class,'createUserFiles'])->name('createUserFiles');
        Route::get('/get-user-files/{user}',[FileController::class,'getUserFiles'])->name('getUserFiles');
        
        Route::get('/test',[FileController::class,'test2'])->name('getFileByLink1s');
        Route::post('/upload-file/{user}',[FileController::class,'uploadFileToUser'])->name('uploadFileToUser');
        Route::delete('/delete-file/{user}',[FileController::class,'deleteFileToUser'])->name('deletFileToUser');
        Route::post('/upload-folder/{user}',[FileController::class,'uploadFolderToUser']);
        Route::delete('/delete-folder/{user}',[FileController::class,'deleteFolderToUser']);

        //1
        Route::get('/templates1',[FileController::class,'getTemplates1'])->middleware('role:admin');
        
        Route::post('/templates1/upload-file',[FileController::class,'uploadFileTemplate1'])->middleware('role:admin');
        Route::post('/templates1/upload-folder',[FileController::class,'uploadFolderTemplate1'])->middleware('role:admin');
        Route::delete('/templates1/delete-folder',[FileController::class,'deleteFolderTemplate1'])->middleware('role:admin');
        Route::delete('/templates1/delete-file',[FileController::class,'deleteFileTemplate1'])->middleware('role:admin');
        //2
        Route::get('/templates2',[FileController::class,'getTemplates2'])->middleware('role:admin');
        
        Route::post('/templates2/upload-file',[FileController::class,'uploadFileTemplate2'])->middleware('role:admin');
        Route::post('/templates2/upload-folder',[FileController::class,'uploadFolderTemplate2'])->middleware('role:admin');
        Route::delete('/templates2/delete-folder',[FileController::class,'deleteFolderTemplate2'])->middleware('role:admin');
        Route::delete('/templates2/delete-file',[FileController::class,'deleteFileTemplate2'])->middleware('role:admin'); 
        
        //3
        Route::get('/templates3',[FileController::class,'getTemplates3'])->middleware('role:admin');
        
        Route::post('/templates3/upload-file',[FileController::class,'uploadFileTemplate3'])->middleware('role:admin');
        Route::post('/templates3/upload-folder',[FileController::class,'uploadFolderTemplate3'])->middleware('role:admin');
        Route::delete('/templates3/delete-folder',[FileController::class,'deleteFolderTemplate3'])->middleware('role:admin');
        Route::delete('/templates3/delete-file',[FileController::class,'deleteFileTemplate3'])->middleware('role:admin'); 
        
        //1,2,3
        Route::get('/templates1/get-file',[FileController::class,'getFileByLinkTemplate1'])->middleware('role:admin');
        Route::get('/templates2/get-file',[FileController::class,'getFileByLinkTemplate2'])->middleware('role:admin');
        Route::get('/templates3/get-file',[FileController::class,'getFileByLinkTemplate3'])->middleware('role:admin');
    });

    Route::put('/public-document/update',[PublicDocumentController::class,'update'])->name('publicDocumentUpdate');
    Route::get('/public-document/all-files',[PublicDocumentController::class,'getAllFilesByLink'])->name('getFileByLink');


    Route::get('payments/invoice/create',[DocsController::class, 'invoiceCreate']);
    Route::post('payments/invoice/create',[DocsController::class, 'invoiceCreate']);

Route::get('docs/onedrive-link/{user}',[FileController::class,'openOneDriveFileByLink'])->name('getFileByLink');
});
Route::get('docs/document-file/{user}',[FileController::class,'getFileByLink'])->name('getFileByLink');
Route::get('docs/google-document-file/{user}',[FileController::class,'getGoogleFileByLink'])->name('getFileByLink');
Route::get('docs/google-document-file-public/{token}',[FileController::class,'getGoogleFileByLinkPublic'])->name('getFileByLink');
Route::get('docs/google-document-file-open/{user}',[FileController::class,'openGoogleFileByLink'])->name('getFileByLink');
Route::get('docs/google-document-zip/{user}',[FileController::class,'openGoogleZip'])->name('getFileByLink');


Route::get('public-document/get',[PublicDocumentController::class,'getDocuments'])->name('getDocuments');
Route::get('/public-document/file',[PublicDocumentController::class,'getFileByLink'])->name('getFileByLink');

//Docs

Route::get('/',[DocsController::class, 'index']);
Route::get('/store/{user}',[DocsController::class, 'store']);
Route::get('/data-change',[DocsController::class, 'dataChange']);

// MAKE EMPTY

Route::get('/empty/{user}',[UserController::class, 'emptyData']);

Route::get('refresh',[OneDriveController::class,'refresh']);

